package by.training;

/**
 * Main application class.
 *
 * @author Max_Bondarev
 * @version 1.0
 */
public class Main {
    /**
     * Application start point.
     *
     * @param programArguments Command line arguments.
     */
    public static void main(String[] programArguments) {
        Input.startInput();
    }
}