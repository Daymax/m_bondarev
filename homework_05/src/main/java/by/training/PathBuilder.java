package by.training;

import by.training.pathitems.File;
import by.training.pathitems.Folder;
import by.training.pathitems.PathItem;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Path builder class.
 * Used for build file system tree from strings.
 *
 * @author Max_Bondarev
 * @version 1.0
 */
public class PathBuilder {
    private Folder path = new Folder("root/");

    /**
     * Adds path to current file system tree.
     *
     * @param pathString path string
     */
    public void addPath(String pathString) {
        ArrayDeque<PathItem> pathItemArrayDeque = getStructureArray(pathString);
        Folder parentItem = path;
        while (pathItemArrayDeque.iterator().hasNext()) {
            PathItem currentItem = pathItemArrayDeque.pop();

            boolean hasFound = false;

            if (currentItem.isFolder()) {
                for (PathItem pathItem : parentItem.getChildren()) {
                    if (pathItem.getName().equals(currentItem.getName())) {
                        parentItem = (Folder) pathItem;
                        hasFound = true;
                    }
                }
                if (!hasFound) {
                    parentItem.add(currentItem);
                    parentItem = (Folder) currentItem;
                }
            } else {
                parentItem.add(currentItem);
            }
        }
    }

    /**
     * Method for get string with current file system tree.
     *
     * @return string with file system tree
     */
    public String getFileSystemTree() {
        return path.getFileSystemTree();
    }

    /**
     * Builds Array Deque with Path Items from path string.
     *
     * @param string path string
     * @return Array Deque with Path Items
     *
     * @see PathItem
     * @see ArrayDeque
     */
    private static ArrayDeque<PathItem> getStructureArray(String string) {
        checkString(string);
        List<String> structure = Arrays.stream(string.split("/")).collect(Collectors.toList());
        ArrayDeque<PathItem> pathItemList = new ArrayDeque<>();
        StringBuilder tabulate = new StringBuilder();
        for (String item : structure) {
            if (item.contains(".")) {
                pathItemList.addLast(new File(tabulate + item));
            } else {
                pathItemList.addLast(new Folder(tabulate + item + "/"));
            }
            tabulate.append("\t");
        }
        checkStructure(pathItemList);
        pathItemList.pop(); //Throwing away the "root" element
        return pathItemList;
    }

    /**
     * Checks string for correctness.
     *
     * @param string path string
     */
    private static void checkString(String string) {
        if (StringUtils.countMatches(string, ".") > 1) {
            throw new InputMismatchException("The string must contain only one File!");
        }
        if (string.equals(StringUtils.EMPTY)) {
            throw new InputMismatchException("The string is empty!");
        }
    }

    /**
     * Checks path structure for correctness.
     *
     * @param structure Array Deque with Path Items
     */
    private static void checkStructure(ArrayDeque<PathItem> structure) {
        if (!structure.getFirst().getName().equals("root/")) {
            throw new InputMismatchException("The string must be in format \"root/*\"");
        }
        for (PathItem item : structure) {
            if (!structure.getLast().getName().contains(".") && item.getName().contains(".")) {
                throw new InputMismatchException("File must be in the end of the string");
            }
        }
    }
}
