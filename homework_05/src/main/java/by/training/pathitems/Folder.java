package by.training.pathitems;

import java.util.LinkedHashSet;

/**
 * Folder class.
 *
 * @see PathItem
 * @author Max_Bondarev
 * @version 1.0
 */
public class Folder implements PathItem {
    private LinkedHashSet<PathItem> children = new LinkedHashSet<>();

    private String name;

    public LinkedHashSet<PathItem> getChildren() {
        return children;
    }

    public String getName() {
        return name;
    }

    public Folder(String name) {
        this.name = name;
    }

    @Override
    public String getFileSystemTree() {
        StringBuilder structure = new StringBuilder(name)
                .append(System.getProperty("line.separator"));
        for (PathItem child : children) {
            structure.append(child.getFileSystemTree());
        }
        return structure.toString();
    }

    /**
     * Adds path item to children list.
     * @see PathItem
     */
    public void add(PathItem pathItem) {
        children.add(pathItem);
    }
}
