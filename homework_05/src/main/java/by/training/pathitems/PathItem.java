package by.training.pathitems;

/**
 * Path item interface.
 *
 * @author Max_Bondarev
 * @version 1.0
 */
public interface PathItem {
    /**
     * Method for get string with file system tree.
     *
     * @return string with file system tree
     */
    String getFileSystemTree();

    /**
     * Method for get path's item's name.
     *
     * @return item's name
     */
    String getName();

    /**
     * Method to determine is the current item Folder.
     *
     * @see Folder
     * @return boolean value
     */
    default boolean isFolder() {
        return this instanceof Folder;
    }
}
