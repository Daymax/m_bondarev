package by.training;

import org.apache.commons.lang3.StringUtils;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Input utility class.
 * Serves to organize input/output file system structure.
 *
 * @author Max_Bondarev
 * @version 1.0
 */
public class Input {
    /**
     * Method for start input path string.
     */
    public static void startInput() {
        PathBuilder builder = new PathBuilder();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please, input path in format \"root/*\"");
        System.out.println("Input \"print\" to print path.");
        System.out.println("To exit the program, input \"exit\".\n");

        String input = StringUtils.EMPTY;
        while (!input.equals("exit")) {
            input = scanner.nextLine();
            if (!input.equals("print") && !input.equals("exit")) {
                try {
                    builder.addPath(input);
                } catch (InputMismatchException exception) {
                    System.err.println(exception.getMessage());
                }
            }
            if (input.equals("print")) {
                System.out.println(builder.getFileSystemTree());
            }
        }
        scanner.close();
    }
}
