package by.training;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.InputMismatchException;

/**
 * PathBuilder test class.
 *
 * @author Max_Bondarev
 * @version 1.0
 */
public class PathBuilderTest {
    private PathBuilder pathBuilder;

    @Before
    public void setUp() {
        pathBuilder = new PathBuilder();
    }

    @Test
    public void testBuildStructure() {
        pathBuilder.addPath("root/folder1/file.txt");
        pathBuilder.addPath("root/folder2/file.txt");
        String lineSeparator = System.getProperty("line.separator");
        String expected = "root/" + lineSeparator
                + "\tfolder1/" + lineSeparator
                + "\t\tfile.txt" + lineSeparator
                + "\tfolder2/" + lineSeparator
                + "\t\tfile.txt" + lineSeparator;
        Assert.assertEquals(expected, pathBuilder.getFileSystemTree());
    }

    @Test
    public void testBuildStructureSameFileNames() {
        pathBuilder.addPath("root/folder1/file.txt");
        pathBuilder.addPath("root/folder1/file.txt");
        String lineSeparator = System.getProperty("line.separator");
        String expected = "root/" + lineSeparator
                + "\tfolder1/" + lineSeparator
                + "\t\tfile.txt" + lineSeparator;
        Assert.assertEquals(expected, pathBuilder.getFileSystemTree());
    }

    @Test(expected = InputMismatchException.class)
    public void testBuildStructureMissRoot_ExceptionInputMismatchException() {
        pathBuilder.addPath("file.txt");
        String lineSeparator = System.getProperty("line.separator");
        String expected = "root/" + lineSeparator
                + "\tfile.txt" + lineSeparator;
        Assert.assertEquals(expected, pathBuilder.getFileSystemTree());
    }

    @Test(expected = InputMismatchException.class)
    public void testBuildStructureMoreThanOneFile_ExceptionInputMismatchException() {
        pathBuilder.addPath("root/folder1/file.txt/file.txt");
        String lineSeparator = System.getProperty("line.separator");
        String expected = "root/" + lineSeparator
                + "\tfile.txt" + lineSeparator;
        Assert.assertEquals(expected, pathBuilder.getFileSystemTree());
    }

    @Test(expected = InputMismatchException.class)
    public void testBuildStructureFileBetweenFolders_ExceptionInputMismatchException() {
        pathBuilder.addPath("root/folder1/file.txt/folder/");
        String lineSeparator = System.getProperty("line.separator");
        String expected = "root/" + lineSeparator
                + "\tfile.txt" + lineSeparator;
        Assert.assertEquals(expected, pathBuilder.getFileSystemTree());
    }
}
