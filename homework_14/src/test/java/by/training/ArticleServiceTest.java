package by.training;

import by.training.exceptions.RequestException;
import by.training.strategies.ConnectionStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ArticleServiceTest {
    @Mock
    private ConnectionStrategy connectionStrategy;
    @InjectMocks
    private ArticleService articleService;

    @Test
    public void testArticleServiceGet() throws IOException {

//      given:
        int id = 1;
        Map<String, Object> urlParameters = new HashMap<>();
        urlParameters.put("id", id);
        String response = "[{\n" +
                "    \"id\": 1,\n" +
                "    \"userId\": 1,\n" +
                "    \"title\": \"Im your father!\",\n" +
                "    \"body\": \"Noooooooo\"\n" +
                "}]";

        when(connectionStrategy.doGet(anyString(), eq(urlParameters))).thenReturn(response);

//      when:
        Optional<Article> expected = Optional.ofNullable(articleService.getArticle(id));

//      then:
        assertTrue(expected.isPresent());
        assertEquals(expected.get().getId(), id);

        verify(connectionStrategy).doGet(anyString(), eq(urlParameters));
    }

    @Test
    public void testArticleServicePost() throws IOException {

//      given:
        int id = 101;
        String jsonStr = "{\"id\":101,\"userId\":1,\"title\":\"Im your father!\",\"body\":\"Noooooooo\"}";
        String response = "[{\n" +
                "    \"id\": 101,\n" +
                "    \"userId\": 1,\n" +
                "    \"title\": \"Im your father!\",\n" +
                "    \"body\": \"Noooooooo\"\n" +
                "}]";

        when(connectionStrategy.doPost(anyString(), eq(jsonStr))).thenReturn(response);

//      when:
        Optional<Article> expected = Optional.ofNullable(articleService.postArticle(id));

//      then:
        assertTrue(expected.isPresent());
        assertEquals(expected.get().getId(), id);

        verify(connectionStrategy).doPost(anyString(), eq(jsonStr));
    }

    @Test(expected = RequestException.class)
    public void testArticleServiceGetWithIOException() throws IOException {

//      given:
        int id = 1;
        Map<String, Object> urlParameters = new HashMap<>();
        urlParameters.put("id", id);

        when(connectionStrategy.doGet(anyString(), eq(urlParameters))).thenThrow(IOException.class);

//      when:
        articleService.getArticle(1);

//      then:
        verify(connectionStrategy).doGet(anyString(), eq(urlParameters));
    }

    @Test(expected = RequestException.class)
    public void testArticleServicePostWithIOException() throws IOException {

//      given:
        String jsonStr = "{\"id\":101,\"userId\":1,\"title\":\"Im your father!\",\"body\":\"Noooooooo\"}";

        when(connectionStrategy.doPost(anyString(), eq(jsonStr))).thenThrow(IOException.class);

//      when:
        articleService.postArticle(101);

//      then:
        verify(connectionStrategy).doPost(anyString(), eq(jsonStr));
    }
}