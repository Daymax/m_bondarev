package by.training;


/**
 * Article entity class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class Article {
    private int id;

    public Article(int id, int userId, String title, String body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    private int userId;
    private String title;
    private String body;

    @Override
    public String toString() {
        return  "User [" + userId + "] "
                + "Title [\"" + title + "\"] "
                + "Message [\"" + body + "\"] ";
    }
}
