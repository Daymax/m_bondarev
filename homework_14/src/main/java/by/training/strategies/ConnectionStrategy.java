package by.training.strategies;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Connect strategy interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface ConnectionStrategy {
    /**
     * Returns string response from GET request.
     *
     * @param url        url
     * @param parameters map of the parameters of the request
     * @return string response
     * @throws IOException when the request cannot be executed
     */
    String doGet(String url, Map<String, Object> parameters) throws IOException;

    /**
     * Returns string response from POST request.
     *
     * @param url        url
     * @param jsonString json body
     * @return string response
     * @throws IOException when the request cannot be executed
     */
    String doPost(String url, String jsonString) throws IOException;

    /**
     * Returns url with added parameters.
     *
     * @param url        url
     * @param parameters map of parameters of the request
     * @return url with added parameters
     */
    default String getUrlWithParameters(String url, Map<String, Object> parameters) {
        List<String> paramsList = new ArrayList<>();
        parameters.forEach((key, value) -> paramsList.add("?" + key + "=" + value.toString()));
        return url + String.join("&", paramsList);
    }
}
