package by.training.strategies;

import lombok.extern.log4j.Log4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.Map;

/**
 * HttpClient connect strategy class.
 *
 * @author m_bondarev
 * @version 1.0
 * @see ConnectionStrategy
 */
@Log4j
public class HttpClientStrategy implements ConnectionStrategy {
    @Override
    public String doGet(String url, Map<String, Object> parameters) throws IOException {
        String urlWithParameters = getUrlWithParameters(url, parameters);
        HttpClient client = HttpClientBuilder.create().build();
        log.info("Sending GET request to " + urlWithParameters);
        HttpGet request = new HttpGet(urlWithParameters);
        request.addHeader("accept", "application/json");
        HttpResponse response = client.execute(request);
        if (response.getStatusLine().getStatusCode() != 200) {
            log.error("Cannot connect to " + urlWithParameters);
            log.error("Response code: " + response.getStatusLine());
            throw new IOException();
        }
        log.info(response.getStatusLine());
        return IOUtils.toString(response.getEntity().getContent());
    }

    @Override
    public String doPost(String url, String jsonString) throws IOException {
        HttpPost request = new HttpPost(url);
        request.addHeader("accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(jsonString));
        HttpClient client = HttpClientBuilder.create().build();
        log.info("Sending POST request to " + url);
        HttpResponse response = client.execute(request);
        if (response.getStatusLine().getStatusCode() != 201) {
            log.error("Cannot connect to " + url);
            log.error("Response code: " + response.getStatusLine());
            throw new IOException();
        }
        log.info(response.getStatusLine());
        return IOUtils.toString(response.getEntity().getContent());
    }
}
