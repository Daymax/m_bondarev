package by.training;

import by.training.strategies.ConnectionStrategy;
import by.training.strategies.HttpClientStrategy;
import by.training.strategies.ManualStrategy;

/**
 * ConnectionStrategyBuilderClass
 * @author m_bondarev
 * @version 1.0
 */
public class ConnectionStrategyBuilder {
    /**
     * Returns ConnectionStrategy from the commandLine parameter.
     *
     * @param strategyParam commandLine parameter
     * @return ConnectionStrategy
     */
    public static ConnectionStrategy getConnectStrategy(String strategyParam) {
        switch (strategyParam) {
            case "-manual": {
                return new ManualStrategy();
            }
            case "-httpclient": {
                return new HttpClientStrategy();
            }
            default: {
                throw new IllegalArgumentException("Invalid connection method name");
            }
        }
    }
}
