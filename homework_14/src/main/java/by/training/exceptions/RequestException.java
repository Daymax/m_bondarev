package by.training.exceptions;

/**
 * Exception for requests when we can't receive a response on our request {@link RuntimeException}.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class RequestException extends RuntimeException{
    /**
     * Constructor with any message.
     *
     * @param message message.
     */
    public RequestException(String message) {
        super(message);
    }
}
