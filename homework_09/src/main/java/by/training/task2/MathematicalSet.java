package by.training.task2;

import java.util.Set;

/**
 * MathematicalSet utility class.
 * @author m_bondarev
 * @version 1.0
 */
public final class MathematicalSet {

    /**
     * Private constructor for the impossibility of creating an instance of this class.
     */
    private MathematicalSet() {}

    /**
     * Method for combining 2 sets.
     * @param firstSet - first set .
     * @param secondSet - second set.
     * @param <T> transmitted type.
     */
    public static <T> void union(Set<T> firstSet, Set<T> secondSet) {
        firstSet.addAll(secondSet);
    }

    /**
     * Method for intersection 2 sets.
     * @param firstSet - first set .
     * @param secondSet - second set.
     * @param <T> transmitted type.
     */
    public static <T> void intersection(Set<T> firstSet, Set<T> secondSet) {
        firstSet.retainAll(secondSet);
    }

    /**
     * Method for minus 2 sets.
     * @param firstSet - first set .
     * @param secondSet - second set.
     * @param <T> transmitted type.
     */
    public static <T> void minus(Set<T> firstSet, Set<T> secondSet) {
        firstSet.removeAll(secondSet);
    }

    /**
     * Method for difference 2 sets.
     * @param firstSet - first set .
     * @param secondSet - second set.
     * @param <T> transmitted type.
     */
    public static <T> void difference(Set<T> firstSet, Set<T> secondSet) {
        for (T item : secondSet) {
            if (firstSet.contains(item)) {
                firstSet.remove(item);
            } else {
                firstSet.add(item);
            }
        }
    }
}
