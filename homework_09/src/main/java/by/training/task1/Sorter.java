package by.training.task1;

import java.util.Arrays;

/**
 * Sorter utility class.
 * @author m_bondarev
 * @version 1.0
 */
public class Sorter {

    /**
     * Private constructor for the impossibility of creating an instance of this class.
     */
    private Sorter() {
    }

    /**
     * Method for sorting incoming array by the sum of the digits of its numbers.
     * @param array incoming array.
     */
    public static void sort(Integer[] array) {

        Arrays.sort(array, (a, b) -> {
            int sumA = String.valueOf(a)
                    .chars()
                    .map(Character::getNumericValue)
                    .sum();
            int sumB = String.valueOf(b)
                    .chars()
                    .map(Character::getNumericValue)
                    .sum();

            return Integer.compare(sumA, sumB);
        });
    }
}
