package by.training.task1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Class for testing Sorter.
 * @author m_bondarev
 * @version 1.0
 */
public class SorterTest {
    private Integer[] array = {13, 12, 45, 43, 2312, 1231, 231};

    @Test
    public void testSort() {
        Integer[] expected = {12, 13, 231, 43, 1231, 2312, 45};
        Sorter.sort(array);
        Assert.assertEquals(expected, array);
    }
}