package by.training.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * MathematicalSet test utility class.
 * @author m_bondarev
 * @version 1.0
 */

public class MathematicalSetTest {
    Set<Character> s1;
    Set<Character> s2;

    @Before
    public void init() {
        s1 = new HashSet<>();
        s1.add('A');
        s1.add('B');
        s2 = new HashSet<>();
        s2.add('B');
        s2.add('C');
    }

    @Test
    public void testUnion() {
        MathematicalSet.union(s1, s2);
        boolean actual = s1.contains('C');
        Assert.assertTrue(actual);
    }

    @Test
    public void testIntersection() {
        MathematicalSet.intersection(s1, s2);
        char actual = s1.stream()
                        .findFirst()
                        .orElse('n'); // n - nothing
        char expected = 'B';
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testMinus() {
        MathematicalSet.minus(s1, s2);
        char actual = s1.stream()
                .findFirst()
                .orElse('n'); // n - nothing
        char expected = 'A';
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testDifference() {
        MathematicalSet.difference(s1, s2);
        boolean actual = s1.contains('B');
        Assert.assertFalse(actual);
    }
}