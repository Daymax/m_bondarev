package by.training.pathitems;

import java.io.Serializable;
import java.util.Objects;

/**
 * File class.
 *
 * @see PathItem
 * @author Max_Bondarev
 * @version 1.0
 */
public class File implements PathItem, Serializable {
    private String name;

    @Override
    public String getName() {
        return name;
    }

    public File(String name) {
        this.name = name;
    }

    @Override
    public String getFileSystemTree() {
        return name + System.getProperty("line.separator");
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        File file = (File) object;
        return name.equals(file.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
