<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<c:if test="${termsAccepted}">
    <c:redirect url="shop"/>
</c:if>

<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Online shop</title>
</head>
<body>
<div class="container">
    <div class="row justify-content-center" style="margin: 20px">
        <h1>Welcome to Online Shop!</h1>
    </div>
    <div class="row justify-content-center">
        <div class="col-4">
            <form action="shop" method="post">
                <div class="form-group">
                    <input type="text" required="required" class="form-control" id="name" name="username" placeholder="Enter your name">
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" name="termsAccept" id="termsAccept">
                    <label class="form-check-label" for="termsAccept">I agree with the terms of service</label>
                </div>
                <button type="submit" class="btn btn-primary">Enter</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
