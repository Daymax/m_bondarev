package by.training.servlets;

import by.training.domain.Order;
import by.training.services.OrderService;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Checkout page servlet class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@WebServlet(name = "OrderPage", urlPatterns = "order")
public class OrderPageServlet extends HttpServlet {
    private final OrderService orderService = new OrderService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        Optional<String> orderOpt = Optional.ofNullable(request.getParameter("id"));
        int orderId;
        if (orderOpt.isPresent()) {
            orderId = Integer.parseInt(orderOpt.get());
            Optional<Order> order = Optional.ofNullable(orderService.getOrderById(orderId));
            if (order.isPresent()) {
                request.setAttribute("order", orderService.getOrderById(orderId));
                request.setAttribute("orderGoods",orderService.getOrderGoodsById(orderId));
                RequestDispatcher view = request
                        .getRequestDispatcher("/WEB-INF/views/order.jsp");
                view.forward(request, response);
            } else {
                response.sendError(404);
            }
        }
    }
}