package by.training.servlets;

import by.training.services.SessionService;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Logout servlet
 * @author m_bondarev
 * @version 1.0
 */
@WebServlet(name = "LogoutServlet", urlPatterns = "logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        HttpSession session = request.getSession();
        SessionService.invalidateSession(session);
        response.sendRedirect("/online-shop/");
    }
}
