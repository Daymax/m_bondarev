package by.training.services;

import by.training.dao.GoodDao;
import by.training.dao.impl.GoodDaoImpl;
import by.training.domain.Good;
import by.training.exceptions.GoodNotFoundException;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Goods service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class GoodsService {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private final GoodDao goodDao = new GoodDaoImpl();

    /**
     * Returns good by id.
     *
     * @param id id
     * @return good by id
     */
    public Good getGoodById(int id) {
        try {
            return goodDao.getGoodById(id).orElseThrow(GoodNotFoundException::new);
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
        return null;
    }

    /**
     * Returns list of all goods.
     *
     * @return list of goods
     */
    public List<Good> getGoods() {
        try {
            return goodDao.getGoods();
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
        return null;
    }
}
