package by.training.services;

import by.training.dao.UserDao;
import by.training.dao.impl.UserDaoImpl;
import by.training.domain.User;
import by.training.exceptions.UserNotFoundException;
import org.apache.flink.util.OptionalConsumer;

import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class UserService {
    private final UserDao userDao = new UserDaoImpl();
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * Returns user by username.
     *
     * @param username username
     * @return User
     */
    public User getUserByName(String username) {
        try {
            Optional<User> user = userDao.getUserByUsername(username);

            if (user.isPresent()) {
                return user.get();
            }
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
        return null;
    }

    /**
     * Adds user to data source.
     *
     * @param user user
     */
    public void addUser(User user) {
        try {
            userDao.addUser(user);
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
    }
}
