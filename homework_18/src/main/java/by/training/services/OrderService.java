package by.training.services;

import by.training.dao.OrderDao;
import by.training.dao.OrderGoodDao;
import by.training.dao.impl.OrderDaoImpl;
import by.training.dao.impl.OrderGoodDaoImpl;
import by.training.domain.Good;
import by.training.domain.Order;
import by.training.domain.User;
import by.training.exceptions.OrderNotFoundException;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Order service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class OrderService {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private final OrderDao orderDao = new OrderDaoImpl();
    private final OrderGoodDao orderGoodDao = new OrderGoodDaoImpl();

    /**
     * Saves order to data source.
     *
     * @param user       user
     * @param addedGoods session added goods
     */
    public void saveOrder(User user, List<Good> addedGoods) {
        int totalPrice = addedGoods.stream().mapToInt(Good::getPrice).sum();
        try {
            Order order = new Order(user.getId(), totalPrice);
            int generatedOrderId = orderDao.addOrder(order);
            order.setId(generatedOrderId);
            for (Good good : addedGoods) {
                orderGoodDao.addOrderGood(order, good);
            }
            System.out.println();
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
    }

    /**
     * Returns user's orders.
     *
     * @param user user
     * @return list of the orders
     */
    public List<Order> getUserOrders(User user) {
        try {
            return orderDao.getUserOrders(user);
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
        return null;
    }

    /**
     * Returns orders's goods.
     *
     * @param orderId order id
     * @return list of the goods
     */
    public List<Good> getOrderGoodsById(int orderId) {
        try {
            return orderGoodDao.getOrderGoodsById(orderId);
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
        return null;
    }

    /**
     * Returns order by id.
     *
     * @param id id
     * @return order
     */
    public Order getOrderById(int id) {
        try {
            return orderDao.getOrderById(id).orElseThrow(OrderNotFoundException::new);
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
        return null;
    }
}
