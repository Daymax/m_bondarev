package by.training.services;

import by.training.domain.Good;
import by.training.domain.Order;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Session service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class SessionService {
    /**
     * Returns list of items from session.
     *
     * @param session session
     * @return list of items
     */
    public static List<Good> getAddedItems(HttpSession session) {
        Optional<List<Good>> addedItems =
                Optional.ofNullable((List) session.getAttribute("addedItems"));
        return addedItems.orElseGet(ArrayList::new);
    }

    /**
     * Adds item to the list of items of session.
     *
     * @param session session
     * @param good    good
     */
    public static void addItemToSession(HttpSession session, Good good) {
        List<Good> addedItems = SessionService.getAddedItems(session);
        addedItems.add(good);
        session.setAttribute("addedItems", addedItems);
    }

    /**
     * Adds item to the list of items of session.
     *
     * @param session session
     * @param index   index
     */
    public static void removeItemFromSession(HttpSession session, int index) {
        List<Good> addedItems = SessionService.getAddedItems(session);
        addedItems.remove(index);
        session.setAttribute("addedItems", addedItems);
    }

    /**
     * Adds username parameter to the session.
     *
     * @param session  session
     * @param username username
     */
    public static void setUsername(String username, HttpSession session) {
        session.setAttribute("username", username);
    }

    /**
     * Returns username from the session.
     *
     * @param session session
     * @return username
     */
    public static String getUsername(HttpSession session) {
        return (String) Optional.of(session.getAttribute("username")).get();
    }

    /**
     * Adds orders to the session.
     *
     * @param session session
     */
    public static void addOrdersToSession(List<Order> orders, HttpSession session) {
        session.setAttribute("orders",
                Optional.ofNullable(orders).orElseGet(ArrayList::new));
    }

    /**
     * Cleanup session added items.
     *
     * @param session session
     */
    public static void cleanUpSessionAddedItems(HttpSession session) {
        session.setAttribute("addedItems", null);
    }

    /**
     * Invalidates session.
     *
     * @param session session
     */
    public static void invalidateSession(HttpSession session) {
        Optional<HttpSession> sessionOptional = Optional.of(session);
        sessionOptional.get().invalidate();
    }
}