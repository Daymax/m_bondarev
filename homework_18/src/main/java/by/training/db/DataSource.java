package by.training.db;

import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * DataSource class for getting connection to the db {@link JdbcConnectionPool}.
 * @author m_bondarev
 * @version 1.0
 */
public class DataSource {
    private final JdbcConnectionPool connectionPool;

    public DataSource() {
        connectionPool =
                JdbcConnectionPool.create("jdbc:h2:mem:testdb", "sa", "");
    }

    public Connection getConnection() throws SQLException {
        return connectionPool.getConnection();
    }
}
