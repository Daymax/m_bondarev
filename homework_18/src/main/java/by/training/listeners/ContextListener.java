package by.training.listeners;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * ContextListener class.
 * @author m_bondarev
 * @version 1.0
 */
@WebListener
public class ContextListener implements ServletContextListener {
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        setUpDatabase();
    }

    private void setUpDatabase() {
        try {
            Connection connection = DriverManager
                    .getConnection("jdbc:h2:mem:testdb;"
                                    + "INIT=create schema if not exists testdb\\;"
                                    + "RUNSCRIPT FROM 'classpath:/init.sql';"
                                    + "DB_CLOSE_DELAY=-1",
                            "sa",
                            "");
        } catch (SQLException exception) {
            logger.log(Level.SEVERE, exception.toString());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
