package by.training.dao.impl;

import by.training.dao.GoodDao;
import by.training.db.DataSource;
import by.training.domain.Good;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Good DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see GoodDao
 */
public class GoodDaoImpl implements GoodDao {
    private final DataSource dataSource = new DataSource();

    private static final String SELECT_ALL_GOODS_SQL_STATEMENT =
            "select * from goods";

    private static final String SELECT_GOOD_SQL_STATEMENT =
            "select * from goods where id=?";

    @Override
    public Optional<Good> getGoodById(int id) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_GOOD_SQL_STATEMENT)) {
            preparedStatement.setInt(1, id);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    return Optional.ofNullable(new Good(rs.getInt("id"),
                            rs.getString("title"),
                            rs.getInt("price")));
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public List<Good> getGoods() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_ALL_GOODS_SQL_STATEMENT);
             ResultSet rs = preparedStatement.executeQuery()) {
            List<Good> goods = new ArrayList<>();
            while (rs.next()) {
                goods.add(new Good(rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("price")));
            }
            return goods;
        }
    }
}
