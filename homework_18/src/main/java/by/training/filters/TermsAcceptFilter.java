package by.training.filters;

import by.training.domain.User;
import by.training.services.SessionService;
import by.training.services.UserService;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Terms accept filter class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@WebFilter(filterName = "TermsAcceptFilter")
public class TermsAcceptFilter implements Filter {
    private final UserService userService = new UserService();
    private static final String TERMS_ACCEPTED = "termsAccepted";

    @Override
    public void init(FilterConfig config) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;


        HttpSession session = servletRequest.getSession(true);

        Optional<String> reqIsAccepted =
                Optional.ofNullable(servletRequest.getParameter("termsAccept"));
        Optional<Object> sessionIsAccepted =
                Optional.ofNullable(session.getAttribute(TERMS_ACCEPTED));

        if (!sessionIsAccepted.isPresent()) {
            if (reqIsAccepted.isPresent()) {
                session.setAttribute(TERMS_ACCEPTED, Boolean.TRUE);
            } else {
                session.setAttribute(TERMS_ACCEPTED, Boolean.FALSE);
            }
        } else {
            if (reqIsAccepted.isPresent()) {
                session.setAttribute(TERMS_ACCEPTED, Boolean.TRUE);
            }
        }

        if (!(boolean) session.getAttribute(TERMS_ACCEPTED)) {
            RequestDispatcher view = servletRequest
                    .getRequestDispatcher("WEB-INF/errors/TermsNotAcceptedError.jsp");
            view.forward(servletRequest, servletResponse);
        } else {
            Optional<String> username = Optional.ofNullable(request.getParameter("username"));
            if (username.isPresent()) {
                Optional<User> user =
                        Optional.ofNullable(userService.getUserByName(username.get()));
                if (user.isPresent()) {
                    userService.addUser(user.get());
                } else {
                    userService.addUser(new User(username.get(), ""));
                }
                SessionService.setUsername(username.get(), session);
            }
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }
}