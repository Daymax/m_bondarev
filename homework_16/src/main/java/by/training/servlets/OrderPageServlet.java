package by.training.servlets;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * OrderPageServlet class.
 * @author m_bondarev
 * @version 1.0
 * urlPatterns = {/order}
 */
public class OrderPageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/HelloPage.jsp");

        requestDispatcher.forward(request, response);
    }

    /**
     * Method takes parameters of order and shows ready to order.
     *
     * @param request  request {@link HttpServletRequest}.
     * @param response response {@link HttpServletResponse}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     * @throws IOException      when arises problems with input-output stream.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/OrderPage.jsp");
        HttpSession session = request.getSession();
        Optional<String> userName = Optional.of(session.getAttribute("name").toString());
        Optional<List<String>> goods = Optional.ofNullable((List<String>) session.getAttribute("items"));

        if (goods.isPresent()) {
            final double total = goods.get()
                    .stream()
                    .map(element -> element.replaceAll("\\D+\\s\\b", StringUtils.EMPTY))
                    .map(Double::valueOf)
                    .reduce(0d, Double::sum);

            session.setAttribute("goods", goods.get());
            session.setAttribute("name", userName.get());
            session.setAttribute("total", total);

            requestDispatcher.forward(request, response);
        }
    }
}
