package by.training.exceptions;

/**
 * AgreementNotAcceptedException class.
 * {@link RuntimeException}
 */
public class AgreementNotAcceptedException extends RuntimeException {}
