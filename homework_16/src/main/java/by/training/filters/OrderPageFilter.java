package by.training.filters;

import by.training.exceptions.AgreementNotAcceptedException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

/**
 * Filter for OrderPage {@link Filter}.
 */
public class OrderPageFilter implements Filter {
    /**
     * Checks if the "terms of service" button is pressed
     * if not redirects to /error page.
     *
     * Checks if the add item button is pressed
     * if yes redirect to /hello for add item to list
     * .
     * @param servletRequest servlet request {@link ServletRequest}
     * @param servletResponse servlet response {@link ServletResponse}
     * @param filterChain chain filters {@link FilterChain}
     * @throws IOException      when arises problems with input-output stream.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        Optional<String> button = Optional.ofNullable(request.getParameter("button"));
        Optional<String> isAgreed = Optional.ofNullable((String) session.getAttribute("isAgreed"));

        isAgreed.orElseThrow(AgreementNotAcceptedException::new);

        if (button.isPresent()) {
            request.getRequestDispatcher("/hello").forward(request,response);
        } else {
            filterChain.doFilter(request, response);
        }
    }

    /**
     * Method for initialize filter.
     * @param filterConfig filter config {@link FilterConfig}
     * @throws ServletException when servlet can throw when it encounters difficulty.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
