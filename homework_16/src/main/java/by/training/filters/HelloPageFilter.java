package by.training.filters;

import by.training.exceptions.AgreementNotAcceptedException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

/**
 * Filter for HelloPage {@link Filter}.
 */
public class HelloPageFilter implements Filter {

    /**
     * Checks if the "terms of service" button is pressed
     * if not redirects to /error page.
     *
     * @param servletRequest  servlet request {@link ServletRequest}
     * @param servletResponse servlet response {@link ServletResponse}
     * @param filterChain     chain filters {@link FilterChain}
     * @throws IOException      when arises problems with input-output stream.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();

        Optional<String> isAgreed = Optional.ofNullable(request.getParameter("isAgreed"));

        isAgreed.orElseThrow(AgreementNotAcceptedException::new);

        session.setAttribute("isAgreed", isAgreed.get());
        filterChain.doFilter(request, servletResponse);
    }

    /**
     * Method for initialize filter.
     *
     * @param filterConfig filter config {@link FilterConfig}
     * @throws ServletException when servlet can throw when it encounters difficulty.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
