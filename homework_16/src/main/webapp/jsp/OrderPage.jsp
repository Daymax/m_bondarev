<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Online Shop</title>
    </head>
    <body>
        <h2 style="text-align: center">Dear ${name}, your order: </h2>
        <div style="width: 200px; margin: auto">
            <ol style="text-align: left; list-style-type: decimal">
                <c:if test="${goods != null}">
                    <c:forEach items="${goods}" var="good">
                        <li>${good}$</li>
                    </c:forEach>
                </c:if>
                <c:if test="${goods == null}">
                    <h3>You have no orders!</h3>
                </c:if>
            </ol>
            <h4>Total: ${total}$</h4>
        </div>
    </body>
</html>
