<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
   <div style="margin-left: 45%">
       <h1>Oops!</h1>
       <p>You shouldn't be here</p>
       <p>Please, agree with the terms of service first.</p><br/>
       <a href="${pageContext.request.contextPath}/">Start page</a></div>
</body>
</html>