import React from 'react';

export default class TodoItem extends React.Component {
    render() {
        const {title, desc} = this.props.item;
        return (
            <div>
                <h1>{title}</h1>
                <p className={'h5'}>{desc}</p>
            </div>
        );
    }
}