import React from 'react';

import TodoListItem from './todo-list-item';
import './todo-list.css';

const TodoList = ({todos, deleteTodo, editTodo}) => {
    const elements = todos.map((el) => {
        return (
            <li
                key={el.id}
                className="list-group-item">
                <TodoListItem {... el}
                              deleteTodo={() => deleteTodo(el.id)}
                              editTodo={editTodo}/>
            </li>
        );
    });

    return (
        <ul style={{marginTop: 30+'px'}}>
            {elements}
        </ul>
    );
};

export default TodoList;
