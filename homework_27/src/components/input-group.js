import React from 'react';

export default class InputGroup extends React.Component {
    state = {
        title: '',
        desc: ''
    };

    titleChange = (event) => {
        this.setState({
            title: event.target.value
        });
    };

    descChange = (event) => {
        this.setState({
            desc: event.target.value
        });
    };

    submitTodo = (event) => {
        event.preventDefault();
        this.props.addTodo(this.state);
        this.setState({
           title: '',
           desc: ''
        });
    };

    render() {
        return (
            <form className="input-add-form d-flex"
                  onSubmit={this.submitTodo}>
                <input type="text"
                       placeholder="Title"
                       className="form-control"
                       onChange={this.titleChange}
                       value={this.state.title}/>
                <input type="text"
                       placeholder="Description"
                       className="form-control"
                       onChange={this.descChange}
                       value={this.state.desc}/>
                <button type="submit"
                        className="btn btn-outline-primary"
                        style={{width: 300 + 'px'}}>Add the task
                </button>
            </form>
        )
    };
}