import AppHeader from "./app-header";
import InputGroup from "./input-group";
import TodoList from "./todo-list";
import TodoItem from "./TodoItem";

import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";

export default class App extends React.Component {
    state = {
        todoData: [
            {title: 'To buy', desc: 'Buy a pen', id: 1},
            {title: 'To call', desc: 'Call mum', id: 2},
            {title: 'To learn', desc: 'Learn React', id: 3}
        ]
    };

    editTodo = ({id, title, desc}) => {
        this.setState(({todoData}) => {
            const idx = todoData.findIndex((el) => el.id === id);
            const newTodoData = [
                ...todoData.slice(0, idx),
                {title: title, desc: desc, id: id},
                ...todoData.slice(idx + 1)
            ];

            return {
                todoData: newTodoData
            };
        })

    };

    deleteTodo = (id) => {
        this.setState(({todoData}) => {
            const idx = todoData.findIndex((el) => el.id === id);

            const newTodoData = [
                ...todoData.slice(0, idx),
                ...todoData.slice(idx + 1)
            ];

            return {
                todoData: newTodoData
            };
        })

    };

    addTodo = ({title, desc}) => {
        this.setState(({todoData}) => {
            const id = todoData.length + 1;
            const newTodoData = [
                ...todoData,
                {title: title, desc: desc, id: id}
            ];

            return {
                todoData: newTodoData
            };
        })

    };

    render() {
        return (
            <div>
                <Router>
                    <Route exact path='/' render={
                        () => {
                            return (
                                <div>
                                    <AppHeader/>
                                    <InputGroup addTodo={this.addTodo}/>
                                    <TodoList todos={this.state.todoData}
                                              deleteTodo={this.deleteTodo}
                                              editTodo={this.editTodo}/>
                                </div>
                            )
                        }
                    }/>
                    <Route exact path='/item/:id' render={
                        ({match}) => {
                            const {id} = match.params;
                            const idx = this.state.todoData
                                .findIndex((el) => el.id === +id);
                            return <TodoItem item={this.state.todoData[idx]}/>
                        }
                    }/>
                </Router>
            </div>
        );
    };
}