import React from 'react';
import './todo-list-item.css';
import {Link} from "react-router-dom";

export default class TodoListItem extends React.Component {

    state = {
        title: this.props.title,
        desc: this.props.desc,
        id: this.props.id,
        open: false
    };

    titleChange = (event) => {
        this.setState({
            title: event.target.value
        });
    };

    descChange = (event) => {
        this.setState({
            desc: event.target.value
        });
    };

    submitEditedTodo = (event) => {
        event.preventDefault();
        this.props.editTodo(this.state);
    };

    render() {
        const {title, desc, id, deleteTodo} = this.props;
        return (
            <span>
        <span className="todo-list-item">
            <Link to={'item/'+id} >
          <span
              className="todo-list-item-label">
              <b>{title}</b>
          </span>
                </Link>

          <button type="button"
                  className="btn btn-outline-danger btn-sm float-right"
                  onClick={deleteTodo}>
            <i className="fa fa-trash-o"/>
          </button>

          <button type="button"
                  className="btn btn-outline-success btn-sm float-right"
                  data-toggle="collapse"
                  data-target={'#edit' + id}
                  aria-expanded="false"
                  aria-controls={'edit' + id}>
            <i className="fa fa-pencil-square-o"/>
          </button>

          <button type="button"
                  className="btn btn-outline-primary btn-sm float-right"
                  style={{width: 150 + 'px'}}
                  data-toggle="collapse"
                  data-target={'#description' + id}
                  aria-expanded="false"
                  aria-controls={'description' + id}>
              View description
          </button>
        </span>
            <div className="collapse" id={'description' + id}>
              <div className="card card-body">
                  {desc}
              </div>
            </div>

            <div className="collapse" id={'edit' + id}>
              <div className="card card-body">
                              <form className="input-add-form"
                                    onSubmit={this.submitEditedTodo}>
                <input type="text"
                       placeholder="Title"
                       className="form-control"
                       value={this.state.title}
                onChange={this.titleChange}/>
                <textarea className="form-control"
                          id="exampleFormControlTextarea1"
                          rows="3"
                          value={this.state.desc}
                          onChange={this.descChange}/>
                <button type="submit"
                        className="btn btn-outline-secondary"
                        style={{width: 300 + 'px'}}
                        data-toggle="collapse"
                        data-target={'#edit' + id}
                        aria-expanded={this.state.open}
                        aria-controls={'edit' + id}
                        onClick={()=>this.state.open = !this.state.open}>Submit
                </button>
            </form>
              </div>
            </div>
    </span>
        );
    }
};
