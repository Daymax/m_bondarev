import React from 'react';

const AppHeader = () => {
  return <h1 style={{margin: '20px', textAlign: 'center'}}>Todo List</h1>;
};

export default AppHeader;