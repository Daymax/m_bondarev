package by.training.controllers;

import by.training.entities.Good;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
public class CheckoutController {
    @RequestMapping(value = "/checkout")
    public String postShopPage(
            HttpServletRequest request, Principal user,
            Model model) {
        int totalCost = 0;
        List<Good> addedItems = (List) request.getSession().getAttribute("addedItems");
        if (addedItems != null) {
            totalCost = addedItems.stream().mapToInt(Good::getPrice).sum();
        }
        model.addAttribute("totalCost", totalCost);
        model.addAttribute("user", user);
        return "checkout";
    }
}
