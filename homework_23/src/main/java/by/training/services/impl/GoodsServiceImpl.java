package by.training.services.impl;

import by.training.dao.GoodDao;
import by.training.dao.impl.GoodDaoImpl;
import by.training.entities.Good;
import by.training.exceptions.NotFoundException;
import by.training.services.GoodsService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Goods service class.
 *
 * @author m_bondarev
 * @version 1.0
 */

@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {
    private final GoodDao goodDao;

    public GoodsServiceImpl(GoodDaoImpl goodDao) {
        this.goodDao = goodDao;
    }

    @Override
    public Good getGoodById(int id) {
        return goodDao.getGoodById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<Good> getGoods() {
        return goodDao.getGoods();
    }
}
