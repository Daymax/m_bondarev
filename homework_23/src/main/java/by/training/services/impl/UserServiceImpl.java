package by.training.services.impl;

import by.training.dao.UserDao;
import by.training.dao.impl.UserDaoImpl;
import by.training.entities.User;
import by.training.exceptions.NotFoundException;
import by.training.services.UserService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * User service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    public UserServiceImpl(UserDaoImpl userDao) {
        this.userDao = userDao;
    }

    @Override
    public User getUserByName(String username) {
        return userDao.getUserByUsername(username).orElseThrow(NotFoundException::new);
    }

    @Override
    public void addUser(User user) {
        userDao.addUser(user);
    }
}
