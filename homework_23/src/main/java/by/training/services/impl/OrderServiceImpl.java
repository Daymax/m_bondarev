package by.training.services.impl;

import by.training.dao.OrderDao;
import by.training.entities.Good;
import by.training.entities.Order;
import by.training.entities.User;
import by.training.exceptions.NotFoundException;
import by.training.services.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Order service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderDao orderDao;

    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void saveOrder(User user, List<Good> addedGoods) {
        orderDao.saveOrder(user, addedGoods);
    }

    @Override
    public List<Order> getUserOrders(User user) {

        return orderDao.getUserOrders(user).stream()
                .sorted(Comparator.comparing(Order::getId))
                .collect(Collectors.toList());
    }

    @Override
    public Order getOrderById(int id) {
        return orderDao.getOrderById(id).orElseThrow(NotFoundException::new);
    }
}
