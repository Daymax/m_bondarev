package by.training.services;

import by.training.entities.Good;

import java.util.List;

/**
 * Goods service interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface GoodsService {
    /**
     * Returns good by id.
     *
     * @param id id
     * @return good by id
     */
    Good getGoodById(int id);

    /**
     * Returns list of all goods.
     *
     * @return list of goods
     */
    List<Good> getGoods();
}
