package by.training.services;

import by.training.entities.User;

/**
 * User service interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface UserService {

    /**
     * Returns user by username.
     *
     * @param username username
     * @return User
     */
    User getUserByName(String username);

    /**
     * Adds user to data source.
     *
     * @param user user
     */
    void addUser(User user);
}
