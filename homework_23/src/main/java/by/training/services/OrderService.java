package by.training.services;

import by.training.entities.Good;
import by.training.entities.Order;
import by.training.entities.User;

import java.util.List;

/**
 * Order service interface.
 *
 * @author m_bondarev
 * @version 1.0
 */

public interface OrderService {
    /**
     * Saves order to data source.
     *
     * @param user       user
     * @param addedGoods session added goods
     */
    void saveOrder(User user, List<Good> addedGoods);

    /**
     * Returns user's orders.
     *
     * @param user user
     * @return list of the orders
     */
    List<Order> getUserOrders(User user);

    /**
     * Returns order by id.
     *
     * @param id id
     * @return order
     */
    Order getOrderById(int id);
}
