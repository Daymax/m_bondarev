package by.training.dao;

import by.training.entities.Good;
import by.training.entities.Order;
import by.training.entities.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Order DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface OrderDao {
    /**
     * Returns order by id.
     *
     * @param id id
     * @return order by id
     */
    Optional<Order> getOrderById(int id);

    /**
     * Returns user's orders.
     *
     * @param user user
     * @return user's orders
     */
    Set<Order> getUserOrders(User user);

    /**
     * Adds order to data source.
     *
     * @param goods goods
     */
    void saveOrder(User user, List<Good> goods);
}
