package by.training.dao.impl;

import by.training.entities.Good;
import by.training.dao.GoodDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;


/**
 * Good DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see GoodDao
 */
@Repository
public class GoodDaoImpl implements GoodDao {

    private final SessionFactory sessionFactory;

    public GoodDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Good> getGoodById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return Optional.ofNullable(session.get(Good.class, id));
    }

    @Override
    public List<Good> getGoods() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaQuery<Good> criteriaQuery = session.getCriteriaBuilder().createQuery(Good.class);
        Root<Good> goodRoot = criteriaQuery.from(Good.class);
        criteriaQuery.select(goodRoot);
        return session.createQuery(criteriaQuery).getResultList();
    }
}
