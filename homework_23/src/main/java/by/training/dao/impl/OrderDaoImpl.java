package by.training.dao.impl;

import by.training.entities.Good;
import by.training.entities.Order;
import by.training.entities.User;
import by.training.dao.OrderDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Order DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see OrderDao
 */
@Repository
public class OrderDaoImpl implements OrderDao {

    private final SessionFactory sessionFactory;

    public OrderDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Order> getOrderById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return Optional.of(session.get(Order.class, id));
    }

    @Override
    public Set<Order> getUserOrders(User user) {
        Session session = sessionFactory.getCurrentSession();
        User currentUser = session.get(User.class, user.getId());
        return currentUser.getOrders();
    }

    @Override
    public void saveOrder(User user, List<Good> goods) {
        Session session = sessionFactory.getCurrentSession();
        int totalPrice = goods.stream().mapToInt(Good::getPrice).sum();
        Order order = new Order();
        User currentUser = session.get(User.class, user.getId());
        order.setGoods(goods);
        order.setTotalPrice(totalPrice);
        currentUser.getOrders().add(order);
        session.save(currentUser);
        session.save(order);
    }
}