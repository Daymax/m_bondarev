package by.training.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Good entity.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Entity
@Table(name = "goods")
public class Good implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private int price;

    public Good() {
    }

    public Good(String title, int price) {
        this.title = title;
        this.price = price;
    }

    public Good(String title) {
        this.title = title;
    }

    public Good(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Good good = (Good) o;
        return id == good.id &&
                price == good.price &&
                Objects.equals(title, good.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, price);
    }
}
