package word;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.InputMismatchException;
import java.util.Map;
import java.util.TreeMap;

public class GeneralTest {

    private Map<String, Integer> map;
    private String[] arrayString;
    private Sort sort;
    private Builder builder;

    /**
     * Initialize method.
     */

    @Before
    public void init() {
        builder = new Builder();
        sort = new Sort();
        map = new TreeMap<>();
        arrayString = "once upon a time time time".split(" ");
        map.put("always", 1);
        map.put("behind", 1);
        map.put("limbo", 1);
        map.put("half", 2);
    }

    /**
     * Test putToMap when values of arrayString putting to map.
     */

    @Test
    public void putToMapTest() {
        map.clear();
        map.put("once", 1);
        map.put("upon", 1);
        map.put("a", 1);
        map.put("time", 3);
        int expected = map.get("time");
        int actual = sort.putToMap(arrayString).get("time");
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test putToMap when the transmitted array is empty.
     */

    @Test(expected = IllegalArgumentException.class)
    public void putToMapWithIllegalArgumentExceptionTest() {
        map.clear();
        sort.putToMap(new String[]{});
    }

    /**
     * Test buildResult when values and key on the map being built to the output.
     */

    @Test
    public void buildResultTest() {
        String expected = "a: \talways 1\n" +
                "b: \tbehind 1\n" +
                "h: \thalf 2\n" +
                "l: \tlimbo 1\n";
        String actual = builder.buildResult(map);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test buildResult when empties fields are transmitted to the map and triggering IllegalArgumentException.
     */

    @Test(expected = IllegalArgumentException.class)
    public void buildResultIllegalArgumentExceptionTest() {
        map.clear();
        builder.buildResult(map);
    }

    /**
     * Test toLowerAndWithoutChars when string are transmitted with bigCase and with chars.
     */

    @Test
    public void toLowerAndWithoutCharsTest(){
        arrayString = "Once upon a Time, Time, Time".split(" ");
        String expected = "time";
        String actual = sort.toLowerAndWithoutChars(arrayString)[4];
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test toLowerAndWithoutChars when the transmitted array is empty.
     */

    @Test(expected = IllegalArgumentException.class)
    public void toLowerAndWithoutCharsWithIllegalArgumentExceptionTest(){
        sort.toLowerAndWithoutChars(new String[]{});
    }

}