import word.Input;

/**
 * Main class.
 * @author Max_Bondarev
 * @version 1.0
 */

public class Main {

    /**
     * Main method.
     * @param args - arguments.
     */

    public static void main(String[] args) {
        Input.startInput();
    }
}
