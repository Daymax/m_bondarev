package word;

import java.util.Map;
import java.util.TreeMap;

/**
 * Sort class to sort and retrieve words.
 * @author Max_bondarev
 * @version 1.0
 */

public class Sort {

    private Map<String, Integer> words = new TreeMap<>();

    /**
     * Method for filling words Map.
     * @param arrayString - Array with words.
     */

    public Map<String, Integer> putToMap(String[] arrayString) {

        if (arrayString.length == 0) {
            throw new IllegalArgumentException();
        } else {
            for (int val = 0; val < arrayString.length; val++) {
                if (words.containsKey(arrayString[val])) {
                    words.put(arrayString[val], words.get(arrayString[val]) + 1);
                } else {
                    words.put(arrayString[val], 1);
                }
            }
            return words;
        }
    }

    /**
     * Change word to lowercase and delete all chars (,.:;!?"").
     * @param arrayString - Array which have words for change.
     */

    public String[] toLowerAndWithoutChars(String[] arrayString) {

        if (arrayString.length == 0) {
            throw new IllegalArgumentException();
        } else {
            for (int i = 0; i < arrayString.length; i++) {
                arrayString[i] = arrayString[i].replaceAll("[,.:;!?\"\"]", "")
                        .toLowerCase();
            }
            return arrayString;
        }
    }
}
