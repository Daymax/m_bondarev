package word;

import org.apache.commons.lang3.StringUtils;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Input class.
 * @author Max Bondarev
 * @version 1.0
 */

public class Input {

    private static String[] arrayWithLowerAndWithoutChars;
    private static String result;
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Starting method to start a program.
     */

    public static void startInput() {

        String inputString = scanner.nextLine();
        Sort sort = new Sort();
        Builder builder = new Builder();

        if (inputString.isEmpty()) {
            throw new InputMismatchException("Field is empty. Write something");
        } else {
            String[] wordsWithoutSpace = inputString.split(StringUtils.SPACE);
            arrayWithLowerAndWithoutChars = sort.toLowerAndWithoutChars(wordsWithoutSpace);
            result = builder.buildResult(sort.putToMap(arrayWithLowerAndWithoutChars));
        }
    }
}
