package word;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class builder for build a result.
 * @author Max_Bondarev
 * @version 1.0
 */

public class Builder {

    private Set<String> firstChars = new LinkedHashSet<>();

    /**
     * Build string for output.
     * @param mapWithWords - Map for change.
     */

    public String buildResult(Map<String, Integer> mapWithWords) {

        StringBuilder builder = new StringBuilder();

        if (mapWithWords.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            for (Map.Entry<String, Integer> item : mapWithWords.entrySet()) {
                firstChars.add(item.getKey().substring(0, 1));
            }

            for (String firstChar : firstChars) {
                builder.append(firstChar).append(":").append(StringUtils.SPACE);
                for (Map.Entry<String, Integer> word : mapWithWords.entrySet()) {
                    if (word.getKey().startsWith(firstChar)) {
                        builder.append("\t")
                               .append(word.getKey())
                               .append(StringUtils.SPACE)
                               .append(word.getValue())
                               .append(StringUtils.LF);
                    }
                }
            }
            return builder.toString();
        }
    }
}
