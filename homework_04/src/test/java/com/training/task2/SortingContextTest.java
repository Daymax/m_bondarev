package com.training.task2;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Class SortingContextTest for testing.
 * @version 1.0.
 */

public class SortingContextTest {
    private static SortingContext context;
    private int[] array = new int[]{8, 7, 12, 4, 3, 9, 85, 2, 1};

    /**
     * Test method for initialization.
     */

    @BeforeClass
    public static void beforeClassTest(){
        context = new SortingContext(new BubbleSort());
    }

    /**
     * Test method for testing executeStrategy() method.
     */

    @Test
    public void executeStrategyTest() {
        context.executeStrategy(array);
    }

    /**
     * Test method for testing method executeStrategy() when we try to sort array without elements.
     */

    @Test(expected = IllegalArgumentException.class)
    public void executeStrategyWithExceptionTest(){
        int[] array = new int[0];
        context.executeStrategy(array);
    }
}