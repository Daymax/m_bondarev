package com.training.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test class for credit card class.
 */

public class CreditCardTest {

    private Card card;

    /**
     * Before method for initialization.
     */

    @Before
    public void BeforeTest() {
        card = new CreditCard("Valera", BigDecimal.valueOf(150));
    }

    /**
     * Test method for testing withdrawals money.
     */

    @Test
    public void withdrawBalanceTest() {
        BigDecimal actual = card.withdrawBalance(BigDecimal.valueOf(200));
        BigDecimal expected = BigDecimal.valueOf(-50);
        Assert.assertEquals(expected, actual);
    }
}