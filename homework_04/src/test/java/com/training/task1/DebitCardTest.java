package com.training.task1;

import com.training.exceptions.NotEnoughMoneyException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test method for debit card class.
 */

public class DebitCardTest {

    private Card card;

    /**
     * Test method for initialization.
     */

    @Before
    public void BeforeTest() {
        card = new DebitCard("Valera", BigDecimal.valueOf(150));
    }

    /**
     * Test method for withdrawals from a card.
     */

    @Test
    public void withdrawBalanceTest() {
        BigDecimal actual = card.withdrawBalance(BigDecimal.valueOf(100));
        BigDecimal expected = BigDecimal.valueOf(50);
        Assert.assertEquals(expected, actual);

    }

    /**
     * Test method when we try to withdraw more money than we have.
     */

    @Test(expected = NotEnoughMoneyException.class)
    public void withdrawBalanceWithNotEnoughMoneyExceptionTest() {
        BigDecimal actual = card.withdrawBalance(BigDecimal.valueOf(200));
        BigDecimal expected = BigDecimal.valueOf(100);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method when we try to withdraw a negative amount.
     */

    @Test(expected = ArithmeticException.class)
    public void withdrawBalanceWithArithmeticExceptionTest() {
        BigDecimal actual = card.withdrawBalance(BigDecimal.valueOf(-50));
        BigDecimal expected = BigDecimal.valueOf(100);
        Assert.assertEquals(expected, actual);
    }
}