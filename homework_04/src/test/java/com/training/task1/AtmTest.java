package com.training.task1;

import com.training.exceptions.NotEnoughMoneyException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test class for atm class.
 */

public class AtmTest {
    private Atm atm;
    private Card card;

    /**
     * Test method for initialization.
     */

    @Before
    public void BeforeTest() {
        card = new DebitCard("Valera", BigDecimal.valueOf(150));
        atm = new Atm(card);
    }

    /**
     * Test method when we try to withdraw more money than we have.
     */

    @Test(expected = NotEnoughMoneyException.class)
    public void ATMWithNotEnoughMoneyExceptionTest() {
        atm.withdrawFromCard(BigDecimal.valueOf(200));
    }

    /**
     * Test method for testing withdrawals from a card.
     */

    @Test
    public void withdrawFromCardTest() {
        BigDecimal actual = atm.withdrawFromCard(BigDecimal.valueOf(50));
        BigDecimal expected = BigDecimal.valueOf(100);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method for testing add to card.
     */

    @Test
    public void addToCardTest() {
        BigDecimal actual = atm.addToCard(BigDecimal.valueOf(50));
        BigDecimal expected = BigDecimal.valueOf(200);
        Assert.assertEquals(expected, actual);
    }
}