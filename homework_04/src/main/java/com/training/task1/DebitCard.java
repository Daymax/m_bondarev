package com.training.task1;

import com.training.exceptions.NotEnoughMoneyException;

import java.math.BigDecimal;

/**
 * Class Debit card that inherits abstract class Card.
 * @version 1.0.
 * @author Max Bondarev.
 */

public class DebitCard extends Card {

    /**
     * Constructor for Debitcard.
     * @param ownerName - Card owner's name.
     * @param balance - Card balance.
     */

    public DebitCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

    /**
     * Method for withdrawing balance.
     * @param money - Money that's withdrawn.
     * @return - Balance with withdrawn funds or IllegalArgumentException.
     */
    @Override
    public BigDecimal withdrawBalance(BigDecimal money) {
        BigDecimal remainder = balance.subtract(money);
        if (remainder.compareTo(BigDecimal.ZERO) >= 0) {
            return super.withdrawBalance(money);
        } else {
            throw new NotEnoughMoneyException("Not enough money");
        }
    }
}
