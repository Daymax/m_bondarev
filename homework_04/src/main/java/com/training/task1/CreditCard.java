package com.training.task1;

import java.math.BigDecimal;

/**
 * Class Credit card that inherits abstract class Card.
 * @version 1.0.
 * @author Max Bondarev.
 */

public class CreditCard extends Card {

    /**
     * Constructor for Debitcard.
     * @param ownerName - Card owner's name.
     * @param balance - Card balance.
     */

    public CreditCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

    /**
     * Method for withdrawing balance.
     * @param money - Money that's withdrawn.
     * @return - Balance with withdrawn funds.
     */

    @Override
    public BigDecimal withdrawBalance(BigDecimal money) {
        return super.withdrawBalance(money);
    }
}
