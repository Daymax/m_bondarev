package com.training.task1;

import java.math.BigDecimal;

/**
 * Atm utility class.
 * @version 1.0.
 * @author Max Bondarev.
 */

public class Atm {
    private Card card;

    /**
     * Constructor that accept the card.
     * @param card - Specific card to be inserted into the ATMю
     */

    public Atm(Card card) {
        this.card = card;
    }

    /**
     * Method for withdrawing balance.
     * @param money - Money that's withdrawn from card.
     */
    public BigDecimal withdrawFromCard(BigDecimal money) {
        return card.withdrawBalance(money);
    }

    /**
     * Method for adding balance to card.
     * @param money - Money that's added to card.
     */

    public BigDecimal addToCard(BigDecimal money) {
        return card.addBalance(money);
    }
}
