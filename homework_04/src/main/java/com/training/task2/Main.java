package com.training.task2;

/**
 * Main class.
 * @version 1.0
 * @author Max.bondarev
 */

public class Main {
    /**
     * Main method.
     * @param args - Arguments.
     */
    public static void main(String[] args) {
        Sorter sorter = new BubbleSort();
        SortingContext context = new SortingContext(sorter);
        int[] array = new int[]{1, 2, 5, 9, 8, 4};
        context.executeStrategy(array);
    }
}
