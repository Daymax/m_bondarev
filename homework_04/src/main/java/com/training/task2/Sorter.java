package com.training.task2;

/**
 * Sorting interface.
 */

public interface Sorter {
    void sort(int[] array);
}
