package com.training.task2;

import java.util.Arrays;

/**
 * SelectionSort utility class.
 * @version 1.0.
 * @author Max Bondarev.
 */

public class SelectionSort implements Sorter {

    /**
     * Sort method for sorting with selection.
     * @param array - Input array.
     */

    public void sort(int[] array) {

        System.out.println("Selection sorting: ");
        System.out.println("Before:\t" + Arrays.toString(array));

        for (int barrier = 0; barrier < array.length - 1; barrier++) {
            for (int i = barrier + 1; i < array.length; i++) {
                if (array[i] < array[barrier]) {
                    int temp = array[i];
                    array[i] = array[barrier];
                    array[barrier] = temp;
                }
            }
        }

        System.out.println("After:\t" + Arrays.toString(array));
    }
}
