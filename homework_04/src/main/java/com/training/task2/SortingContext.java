package com.training.task2;

/**
 * Class Strategy Context.
 * @version 1.0.
 * @author Max Bondarev.
 */

public class SortingContext {
    private Sorter sorterStrategy;

    /**
     * Constructor for choose strategy sorting.
     * @param sorterStrategy - Choose your strategy sorting.
     */

    public SortingContext(Sorter sorterStrategy) {
        this.sorterStrategy = sorterStrategy;
    }

    /**
     * Execute selected strategy.
     * @param array - Input array for sort.
     */

    public void executeStrategy(int[] array) {
        if (array.length != 0) {
            sorterStrategy.sort(array);
        } else {
            throw new IllegalArgumentException();
        }
    }
}
