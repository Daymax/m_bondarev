package com.training.task2;

import java.util.Arrays;

/**
 * BubbleSort class.
 * @version 1.0.
 * @author Max Bondarev.
 */

public class BubbleSort implements Sorter {

    /**
     * Sort method for sorting with bubble.
     * @param array - Input array.
     */

    public void sort(int[] array) {

        System.out.println("Bubble sorting: ");
        System.out.println("Before:\t" + Arrays.toString(array));

        for (int barrier = array.length - 1; barrier >= 0; barrier--) {
            for (int i = 0; i < barrier; i++) {
                if (array[i] > array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
        System.out.println("After:\t" + Arrays.toString(array));
    }
}
