create table if not exists users
(
    id       INTEGER auto_increment,
    username VARCHAR(255),
    password VARCHAR(255),
    PRIMARY KEY (id)
);

create table if not exists goods
(
    id    INTEGER auto_increment,
    title VARCHAR(255),
    price INTEGER NOT NULL,
    PRIMARY KEY (id)
);

create table if not exists orders
(
    id          INTEGER auto_increment,
    user_id     INTEGER NOT NULL,
    total_price INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) references users (id)
);

create table if not exists order_good
(
    id       INTEGER auto_increment,
    order_id INTEGER,
    good_id  INTEGER,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) references orders (id),
    FOREIGN KEY (good_id) references goods (id)
);

insert into goods(title, price)
values ('Book', 10);
insert into goods(title, price)
values ('Pen', 2);
insert into goods(title, price)
values ('Pencil', 1);
insert into goods(title, price)
values ('Paper', 5);