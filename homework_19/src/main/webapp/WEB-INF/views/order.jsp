<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Online shop</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/online-shop">Online-shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/online-shop/shop">Shop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/online-shop/orders/">My orders</a>
            </li>
        </ul>
        <form action="/online-shop/logout" method="post" class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
        </form>
    </div>
</nav>
<div class="container">
    <div class="row justify-content-center">
        <div style="margin: 20px" class="col-8">
            <p class="h5" style="margin-bottom: 10px">Order #${requestScope.order.id}</p>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="good" items="${requestScope.orderGoods}">
                    <tr>
                        <th scope="row">${good.title}</th>
                        <td>${good.price}$</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <p class="h5" style="margin-top: 20px">Total: ${requestScope.order.totalPrice}$</p>
        </div>
    </div>
</div>
</body>
</html>
