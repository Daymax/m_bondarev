<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Online shop</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/online-shop">Online-shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/online-shop/shop">Shop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/online-shop/orders/">My orders</a>
            </li>
        </ul>
        <form action="/online-shop/logout" method="post" class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
        </form>
    </div>
</nav>
<div class="container">
    <div class="row justify-content-center" style="margin: 20px">
        <h1>Hello ${username}!</h1>
    </div>
    <div class="row justify-content-center" style="margin-bottom: 20px">
        <div class="col-8">
            <p class="h5">Make your order</p>
            <form action="shop" method="post">
                <div class="input-group">

                    <select class="custom-select" name="selectedItem">
                        <c:forEach var="good" varStatus="loop" items="${goods}">
                        <option value="${good.id}">${good.title} - ${good.price}$</option>
                        </c:forEach>
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">Add to cart</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-8">
            <c:if test="${empty addedItems}">
                <p class="h5">You shopping cart is empty!</p>
            </c:if>
            <c:if test="${!empty addedItems}">
                <p class="h5">You have already chosen:</p>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" varStatus="loop" items="${addedItems}">
                        <tr>
                            <th scope="row">${loop.index+1}</th>
                            <td>${item.title}</td>
                            <td>${item.price}$</td>
                            <td style="width: 20%">
                                <form class="form-inline my-2 my-lg-0" action="shop" method="post">
                                    <input hidden name="deletedItem" value="${loop.index}">
                                    <button type="submit" class="btn btn-outline-danger my-2 my-sm-0 ">Delete item
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <form action="checkout" method="post" style="margin: 20px">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
