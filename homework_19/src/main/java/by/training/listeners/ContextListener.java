package by.training.listeners;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ContextListener class.
 * @author m_bondarev
 * @version 1.0
 */
@WebListener
public class ContextListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(ContextListener.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        servletContextEvent.getServletContext().setAttribute("applicationContext", applicationContext);
        setUpDatabase();
    }

    private void setUpDatabase() {
        try {
            Connection connection = DriverManager
                    .getConnection("jdbc:h2:mem:by_training;"
                                    + "INIT=create schema if not exists by_training\\;"
                                    + "RUNSCRIPT FROM 'classpath:/init.sql';"
                                    + "DB_CLOSE_DELAY=-1",
                            "sa",
                            "");
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.toString());
        }
        LOGGER.log(Level.INFO, "[Database connected]");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
