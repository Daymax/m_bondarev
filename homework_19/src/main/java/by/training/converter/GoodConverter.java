package by.training.converter;

import by.training.domain.Good;
import by.training.dto.GoodDto;
import org.springframework.stereotype.Component;

/**
 * GoodConverter class for converting entity to dto or dto to entity.
 * @author m_bondarev
 * @version 1.0
 */
@Component
public class GoodConverter {

    /**
     * Method changes dto {@link GoodDto} to entity.
     * @param dto - dto
     * @return entity from dto
     */
    public Good toEntity(final GoodDto dto) {
        return new Good(dto.getId(), dto.getTitle(), dto.getPrice());
    }

    /**
     * Method changes entity {@link Good} to dto.
     * @param entity - entity
     * @return dto from entity
     */
    public GoodDto toDto(final Good entity) {
        return new GoodDto(entity.getId(), entity.getTitle(), entity.getPrice());
    }

    /**
     * Method enriches an existing one entity {@link Good}.
     * @param entity entity
     * @param dto dto
     * @return good from dto {@link GoodDto}
     */
    public Good enrich(final Good entity, final GoodDto dto) {
        entity.setId(dto.getId());
        entity.setTitle(dto.getTitle());
        entity.setPrice(dto.getPrice());
        return entity;
    }
}
