package by.training.converter;

import by.training.domain.User;
import by.training.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public User toEntity(final UserDto dto) {
        return new User(dto.getId(), dto.getUsername());
    }

    public UserDto toDto(final User entity) {
        if (entity != null) {
            return new UserDto(entity.getId(), entity.getUsername());
        }
        return null;
    }

    public User enrich(final User entity, final UserDto dto) {
        entity.setUsername(dto.getUsername());
        entity.setId(dto.getId());
        return entity;
    }
}
