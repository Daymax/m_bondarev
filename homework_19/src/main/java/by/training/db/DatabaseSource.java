package by.training.db;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * DataSource class for getting connection to the db {@link JdbcTemplate}.
 * @author m_bondarev
 * @version 1.0
 */
public class DatabaseSource {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private static final String JDBC_URL = "jdbc:h2:mem:by_training";
    private static final String JDBC_LOGIN = "sa";
    private static final String JDBC_PASSWORD = StringUtils.EMPTY;

    public DatabaseSource() {
        DataSource dataSource = new DriverManagerDataSource(JDBC_URL, JDBC_LOGIN, JDBC_PASSWORD);
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Connection getConnection() throws SQLException {
        return jdbcTemplate.getDataSource().getConnection();
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}

