package by.training.dto;

/**
 * UserDto class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class UserDto {
    private Integer id;
    private String username;

    public UserDto() {
    }

    public UserDto(String username) {
        this.username = username;
    }

    public UserDto(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
