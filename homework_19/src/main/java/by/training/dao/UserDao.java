package by.training.dao;

import by.training.domain.User;

import java.sql.SQLException;

/**
 * User DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface UserDao {
    /**
     * Adds user to data source.
     * @param user user
     * @throws SQLException when gets data source error
     */
    void addUser(User user) throws SQLException;

    /**
     * Returns user by username.
     * @param username username
     * @throws SQLException when gets data source error
     */
    User getUserByUsername(String username) throws SQLException;
}
