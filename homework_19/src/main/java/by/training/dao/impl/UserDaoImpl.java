package by.training.dao.impl;

import by.training.dao.UserDao;
import by.training.db.DatabaseSource;
import by.training.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see UserDao
 */
public class UserDaoImpl implements UserDao {
    private DatabaseSource databaseSource;

    private static final String MERGE_USER_SQL_STATEMENT =
            "insert into users(username,password) values (?,?)";

    private static final String SELECT_USER_SQL_STATEMENT =
            "select * from users where username=?";

    @Override
    public void addUser(User user) throws SQLException {

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(MERGE_USER_SQL_STATEMENT)) {

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public User getUserByUsername(String username) throws SQLException {

        try (Connection connection = databaseSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_USER_SQL_STATEMENT)) {

            preparedStatement.setString(1, username);

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    return new User(rs.getInt("id"),
                                    rs.getString("username"),
                                    rs.getString("password"));
                }
                return null;
            }
        }
    }

    public void setDatabaseSource(DatabaseSource databaseSource) {
        this.databaseSource = databaseSource;
    }
}
