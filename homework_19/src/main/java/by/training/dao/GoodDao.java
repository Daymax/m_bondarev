package by.training.dao;

import by.training.domain.Good;

import java.sql.SQLException;
import java.util.List;

/**
 * Good DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface GoodDao {
    /**
     * Returns good by id.
     * @param id id
     * @return good by id
     * @throws SQLException when gets data source error
     */
    Good getGoodById(int id) throws SQLException;

    /**
     * Returns list of all goods.
     * @return list of goods
     * @throws SQLException when gets data source error
     */
    List<Good> getGoods() throws SQLException;
}
