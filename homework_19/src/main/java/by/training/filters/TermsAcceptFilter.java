package by.training.filters;

import by.training.converter.UserConverter;
import by.training.domain.User;
import by.training.dto.UserDto;
import by.training.services.SessionService;
import by.training.services.UserService;
import org.springframework.context.ApplicationContext;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;


/**
 * Terms accept filter class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@WebFilter(filterName = "TermsAcceptFilter")
public class TermsAcceptFilter implements Filter {
    private UserService userService;
    private UserConverter userConverter;

    private static final String TERMS_ACCEPTED = "termsAccepted";

    @Override
    public void init(FilterConfig config) {
        ApplicationContext context =
                (ApplicationContext) config.getServletContext().getAttribute("applicationContext");
        userService = (UserService) context.getBean("userService");
        userConverter = (UserConverter) context.getBean("userConverter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;

        HttpSession session = servletRequest.getSession(true);

        Optional<String> reqIsAccepted =
                Optional.ofNullable(servletRequest.getParameter("termsAccept"));
        Optional<Object> sessionIsAccepted =
                Optional.ofNullable(session.getAttribute(TERMS_ACCEPTED));

        if (!sessionIsAccepted.isPresent()) {
            if (reqIsAccepted.isPresent()) {
                session.setAttribute(TERMS_ACCEPTED, Boolean.TRUE);
            } else {
                session.setAttribute(TERMS_ACCEPTED, Boolean.FALSE);
            }
        } else {
            if (reqIsAccepted.isPresent()) {
                session.setAttribute(TERMS_ACCEPTED, Boolean.TRUE);
            }
        }

        if (!(boolean) session.getAttribute(TERMS_ACCEPTED)) {
            RequestDispatcher view = servletRequest
                    .getRequestDispatcher("WEB-INF/errors/TermsNotAcceptedError.jsp");
            view.forward(servletRequest, servletResponse);
        } else {
            Optional<String> username = Optional.ofNullable(request.getParameter("username"));
            if (username.isPresent()) {
                Optional<UserDto> user =
                        Optional.ofNullable(userService.getUserByName(username.get()));
                if (user.isPresent()) {
                    userService.addUser(userConverter.toEntity(user.get()));
                } else {
                    userService.addUser(new User(username.get(), ""));
                }
                SessionService.setUsername(username.get(), session);
            }
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}