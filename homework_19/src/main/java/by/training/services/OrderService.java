package by.training.services;

import by.training.converter.GoodConverter;
import by.training.converter.OrderConverter;
import by.training.converter.UserConverter;
import by.training.dao.OrderDao;
import by.training.dao.OrderGoodDao;
import by.training.dto.GoodDto;
import by.training.dto.OrderDto;
import by.training.dto.UserDto;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Order service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class OrderService {
    private static final Logger LOGGER = Logger.getLogger(OrderService.class.getName());
    private OrderDao orderDao;
    private OrderGoodDao orderGoodDao;
    private GoodConverter goodConverter;
    private OrderConverter orderConverter;
    private UserConverter userConverter;

    public OrderService(OrderDao orderDao, OrderGoodDao orderGoodDao, GoodConverter goodConverter,
                        OrderConverter orderConverter, UserConverter userConverter) {
        this.orderDao = orderDao;
        this.orderGoodDao = orderGoodDao;
        this.goodConverter = goodConverter;
        this.orderConverter = orderConverter;
        this.userConverter = userConverter;
    }

    /**
     * Saves order to data source.
     *
     * @param userDto       user
     * @param addedGoods session added goods
     */
    public void saveOrder(UserDto userDto, List<GoodDto> addedGoods) {
        int totalPrice = addedGoods.stream().mapToInt(GoodDto::getPrice).sum();
        try {
            OrderDto orderDto = new OrderDto(userDto.getId(), totalPrice);
            int generatedOrderId = orderDao.addOrder(orderConverter.toEntity(orderDto));
            orderDto.setId(generatedOrderId);
            for (GoodDto goodDto : addedGoods) {
                orderGoodDao.addOrderGood(orderConverter.toEntity(orderDto), goodConverter.toEntity(goodDto));
            }
            System.out.println();
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.toString());
        }
    }

    /**
     * Returns user's orders.
     *
     * @param userDto user
     * @return list of the orders
     */
    public List<OrderDto> getUserOrders(UserDto userDto) {
        try {
            return orderDao.getUserOrders(userConverter.toEntity(userDto)).stream()
                                                                          .map(order -> orderConverter.toDto(order))
                                                                          .collect(Collectors.toList());
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        return null;
    }

    /**
     * Returns orders's goods.
     *
     * @param orderId order id
     * @return list of the goods
     */
    public List<GoodDto> getOrderGoodsById(int orderId) {
        try {
            return orderGoodDao.getOrderGoodsById(orderId).stream()
                                                          .map(good -> goodConverter.toDto(good))
                                                          .collect(Collectors.toList());
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        return null;
    }

    /**
     * Returns order by id.
     *
     * @param id id
     * @return order
     */
    public OrderDto getOrderById(int id) {
        try {
            return orderConverter.toDto(orderDao.getOrderById(id));
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        return null;
    }
}
