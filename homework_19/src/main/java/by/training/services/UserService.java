package by.training.services;

import by.training.converter.UserConverter;
import by.training.dao.UserDao;
import by.training.domain.User;
import by.training.dto.UserDto;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class UserService {
    private UserDao userDao;
    private UserConverter userConverter;
    private static final Logger LOGGER = Logger.getLogger(UserService.class.getName());

    public UserService(UserDao userDao, UserConverter userConverter) {
        this.userDao = userDao;
        this.userConverter = userConverter;
    }

    /**
     * Returns user by username.
     *
     * @param username username
     * @return User
     */
    public UserDto getUserByName(String username) {
        try {
            return userConverter.toDto(userDao.getUserByUsername(username));
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        return null;
    }

    /**
     * Adds user to data source.
     *
     * @param user user
     */
    public void addUser(User user) {
        try {
            userDao.addUser(user);
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
    }
}
