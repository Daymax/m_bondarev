package by.training.domain;

/**
 * Good class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class Good {
    private Integer id;
    private String title;
    private Integer price;

    public Good() {}

    public Good(String title) {
        this.title = title;
    }

    public Good(String title, int price) {
        this.title = title;
        this.price = price;
    }

    public Good(int id, String title, int price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
