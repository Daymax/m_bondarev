package by.training.servlets;

import by.training.dto.OrderDto;
import by.training.services.OrderService;
import org.springframework.context.ApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Checkout page servlet class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@WebServlet(name = "OrderPage", urlPatterns = "order")
public class OrderPageServlet extends HttpServlet {
    private OrderService orderService;

    @Override
    public void init(ServletConfig config) {
        final ApplicationContext context =
                (ApplicationContext) config.getServletContext()
                                           .getAttribute("applicationContext");
        orderService = (OrderService) context.getBean("orderService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        Optional<String> orderOpt = Optional.ofNullable(request.getParameter("id"));
        int orderId;
        if (orderOpt.isPresent()) {
            orderId = Integer.parseInt(orderOpt.get());
            Optional<OrderDto> orderDto = Optional.ofNullable(orderService.getOrderById(orderId));
            if (orderDto.isPresent()) {
                request.setAttribute("order", orderService.getOrderById(orderId));
                request.setAttribute("orderGoods",orderService.getOrderGoodsById(orderId));
                RequestDispatcher view = request
                        .getRequestDispatcher("/WEB-INF/views/order.jsp");
                view.forward(request, response);
            } else {
                response.sendError(404);
            }
        }
    }
}