package by.training.servlets;

import by.training.services.OrderService;
import by.training.services.SessionService;
import by.training.services.UserService;
import org.springframework.context.ApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

/**
 * Checkout page servlet class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@WebServlet(name = "OrdersPage", urlPatterns = "orders/")
public class OrdersPageServlet extends HttpServlet {
    private OrderService orderService;
    private UserService userService;

    @Override
    public void init(ServletConfig config) {
        final ApplicationContext context =
                (ApplicationContext) config.getServletContext()
                                           .getAttribute("applicationContext");

        orderService = (OrderService) context.getBean("orderService");
        userService = (UserService) context.getBean("userService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();

        String username = SessionService.getUsername(session);
        SessionService.addOrdersToSession(orderService
                .getUserOrders(userService.getUserByName(username)), session);
        RequestDispatcher view = request
                .getRequestDispatcher("/WEB-INF/views/orders.jsp");
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();

        String username = SessionService.getUsername(session);
        Optional<String> finishOrder = Optional.ofNullable(request.getParameter("finishOrder"));
        if (finishOrder.isPresent()) {
            orderService.saveOrder(
                    userService.getUserByName(username), SessionService.getAddedItems(session)
            );
            SessionService.addOrdersToSession(orderService
                    .getUserOrders(userService.getUserByName(username)), session);
            SessionService.cleanUpSessionAddedItems(session);
        }
        RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/views/orders.jsp");
        view.forward(request, response);

    }
}