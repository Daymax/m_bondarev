package by.training.servlets;

import by.training.services.GoodService;
import by.training.services.SessionService;
import org.springframework.context.ApplicationContext;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

/**
 * Order page servlet class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@WebServlet(name = "ShopPage", urlPatterns = "shop")
public class ShopPageServlet extends HttpServlet {

    private GoodService goodService;

    @Override
    public void init(ServletConfig config) {
        final ApplicationContext context =
                (ApplicationContext) config.getServletContext()
                                           .getAttribute("applicationContext");
        goodService = (GoodService) context.getBean("goodService");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();

        Optional<String> deletedItemIndex =
                Optional.ofNullable(request.getParameter("deletedItem"));
        deletedItemIndex.ifPresent(itemIndex -> SessionService
                .removeItemFromSession(session, Integer.parseInt(itemIndex)));

        Optional<String> selectedItemId =
                Optional.ofNullable(request.getParameter("selectedItem"));
        selectedItemId.ifPresent(itemId -> SessionService
                        .addItemToSession(session, goodService
                                .getGoodById(Integer.parseInt(itemId))));
        session.setAttribute("goods", goodService.getGoods());

        RequestDispatcher view =
                request.getRequestDispatcher("/WEB-INF/views/shop.jsp");
        view.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        RequestDispatcher view =
                request.getRequestDispatcher("/WEB-INF/views/shop.jsp");
        view.forward(request, response);
    }
}