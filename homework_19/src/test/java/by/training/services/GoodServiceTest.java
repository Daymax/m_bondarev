package by.training.services;

import by.training.converter.GoodConverter;
import by.training.dao.GoodDao;
import by.training.domain.Good;
import by.training.dto.GoodDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GoodServiceTest {

    @Mock
    private GoodDao goodDao;
    @Mock
    private GoodConverter goodConverter;
    @InjectMocks
    private GoodService goodService;

    @Test
    public void testGetGoodById() throws SQLException {

//      given:
        int id = 1;
        Good good = new Good("Book", 10);
        GoodDto goodDto = new GoodDto("Book", 10);

        when(goodConverter.toDto(good)).thenReturn(goodDto);
        when(goodDao.getGoodById(id)).thenReturn(good);

//      when:
        Optional<GoodDto> actual = Optional.ofNullable(goodService.getGoodById(id));

//      then:
        assertTrue(actual.isPresent());
        assertEquals(goodDto, actual.get());

        verify(goodDao).getGoodById(id);
        verify(goodConverter).toDto(good);
    }

    @Test
    public void testGetGoods() throws SQLException {

//      given:
        Good good = new Good("Book", 10);
        GoodDto goodDto = new GoodDto("Book", 10);
        List<Good> goods = new ArrayList<>();
        goods.add(good);

        when(goodDao.getGoods()).thenReturn(goods);
        when(goodConverter.toDto(good)).thenReturn(goodDto);

//      when:
        Optional<List<GoodDto>> actual = Optional.ofNullable(goodService.getGoods());

//      then:
        assertTrue(actual.isPresent());
        assertEquals(goodDto, actual.get().get(0));

        verify(goodDao).getGoods();
        verify(goodConverter).toDto(good);
    }
}