package by.training.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * DefaultPageServlet class.
 * UrlPatterns = {/}.
 * @author m_bondarev
 * @version 1.0
 */
public class DefaultPageServlet extends HttpServlet {

    /**
     * Method which shows default page.
     *
     * @param request  request {@link HttpServletRequest}.
     * @param response response {@link HttpServletResponse}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     * @throws IOException      when arises problems with input-output stream.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(true);
        session.invalidate();
        session.setMaxInactiveInterval(120);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DefaultPage.jsp");
        requestDispatcher.forward(request, response);
    }
}
