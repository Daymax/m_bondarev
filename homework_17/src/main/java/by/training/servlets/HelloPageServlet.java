package by.training.servlets;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * HelloPageServlet class.
 * urlPatterns = {/hello}
 * @author m_bondarev
 * @version 1.0
 */
public class HelloPageServlet extends HttpServlet {
    //Map with servletContext parameters
    private Map<String, Double> itemNameToPrice;
    //List with our orders
    private final List<String> items = new ArrayList<>();
    /**
     * Method for initialization servlet.
     *
     * @param config - servlet config {@link ServletConfig}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        final ServletContext servletContext = config.getServletContext();
        itemNameToPrice = Collections.list(servletContext.getInitParameterNames())
                .stream()
                .collect(Collectors.toMap(name ->
                        name, name -> Double.valueOf(servletContext.getInitParameter(name))));
        super.init(config);
    }

    /**
     * Method which shows helloPage with items on <select>.
     *
     * @param request  request {@link HttpServletRequest}.
     * @param response response {@link HttpServletResponse}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     * @throws IOException      when arises problems with input-output stream.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Optional<String> button = Optional.ofNullable((String)session.getAttribute("button"));
        Optional<String> userName = Optional.of(request.getParameter("name"));
        String name = userName.get().equals(StringUtils.EMPTY) ? "Anonymous" : userName.get();

        if (button.isPresent()) {
            session.setAttribute("button", button);
            doPost(request, response);
        }

        items.clear();
        session.setAttribute("name", name);
        session.setAttribute("itemNameToPrice", itemNameToPrice);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/HelloPage.jsp");
        requestDispatcher.forward(request, response);
    }

    /**
     * Method takes parameters for add item to list.
     *
     * @param request  request {@link HttpServletRequest}.
     * @param response response {@link HttpServletResponse}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     * @throws IOException      when arises problems with input-output stream.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/HelloPage.jsp");
        Optional<String> thing = Optional.of(request.getParameter("thing"));
        HttpSession session = request.getSession();

        items.add(thing.get()
                .replaceAll("[\\[\\](){}$]", StringUtils.EMPTY));

        session.setAttribute("items", items);
        requestDispatcher.forward(request, response);
    }
}
