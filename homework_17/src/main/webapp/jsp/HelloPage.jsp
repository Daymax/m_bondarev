<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Online Shop</title>
</head>
<body>
<h1 style="text-align: center">Hello ${name}!</h1>
<div style="width: 200px; margin: auto">
        <c:if test="${items != null}">
            <h5 style="text-align: center">You have already choosen: </h5>
            <ol style="text-align: left; list-style-type: decimal">
            <c:forEach items="${items}" var="item">
                <li>${item}$</li>
            </c:forEach>
        </c:if>
        <c:if test="${items == null}">
            <h5 style="text-align: center">Make your order: </h5>
        </c:if>
    </ol>
</div>
<form style="text-align: center" action="/order" method="post">
    <input hidden name="name" value="${name}"><br/>
    <select name="thing">
        <%--@elvariable id="itemNameToPrice" type="java.util.Map"--%>
        <c:forEach items="${itemNameToPrice}" var="item">
            <option>${item.getKey()} (${item.getValue()}$)</option>
        </c:forEach>
    </select><br/>
    <div style="margin-top: 20px">
        <button type="submit" name="button" value="addButton">Add item</button>
        <button type="submit">Submit</button>
    </div>
</form>
</body>
</html>
