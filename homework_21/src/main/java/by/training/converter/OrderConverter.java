package by.training.converter;

import by.training.dto.OrderDto;
import by.training.entities.Order;
import org.springframework.stereotype.Component;

/**
 * OrderConverter class for converting entity to dto or dto to entity.
 * @author m_bondarev
 * @version 1.0
 */
@Component
public class OrderConverter {

    /**
     * Method changes dto {@link OrderDto} to entity.
     * @param dto - dto
     * @return entity from dto
     */
    public Order toEntity(final OrderDto dto) {
        if (dto.getId() != null) {
            return new Order(dto.getId(), dto.getUserId(), dto.getTotalPrice());
        }
        return new Order(dto.getUserId(), dto.getTotalPrice());
    }

    /**
     * Method changes entity {@link Order} to dto.
     * @param entity - entity
     * @return dto from entity
     */
    public OrderDto toDto(final Order entity) {
        return new OrderDto(entity.getId(), entity.getUserId(), entity.getTotalPrice());
    }

    /**
     * Method enriches an existing one entity {@link Order}.
     * @param entity entity
     * @param dto dto
     * @return good from dto {@link OrderDto}
     */
    public Order enrich(final Order entity, final OrderDto dto) {
        entity.setUserId(dto.getUserId());
        entity.setId(dto.getId());
        entity.setTotalPrice(dto.getTotalPrice());
        return entity;
    }
}
