package by.training.converter;

import by.training.dto.UserDto;
import by.training.entities.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public User toEntity(final UserDto dto) {
        return new User(dto.getId(), dto.getUsername(), dto.getRole());
    }

    public UserDto toDto(final User entity) {
        if (entity != null) {
            return new UserDto(entity.getId(), entity.getUsername(), entity.getRole());
        }
        return null;
    }

    public User enrich(final User entity, final UserDto dto) {
        entity.setUsername(dto.getUsername());
        entity.setId(dto.getId());
        return entity;
    }
}
