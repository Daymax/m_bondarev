package by.training.controllers;

import by.training.dto.GoodDto;
import by.training.entities.Good;
import by.training.services.GoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes("addedItems")
public class ShopController {
    private final GoodsService goodsService;

    public ShopController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @RequestMapping(value = "/")
    public ModelAndView getShopPage(Principal user,
                                     @ModelAttribute("addedItems") List<GoodDto> addedItems,
                                     Integer selectedItem,
                                     Integer deletedItem) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("shop.html");
        if (selectedItem != null) {
            addedItems.add(goodsService.getGoodById(selectedItem));
        }
        if (deletedItem != null) {
            addedItems.remove(deletedItem.intValue());
        }
        modelAndView.addObject("addedItems", addedItems);
        modelAndView.addObject("user", user);
        modelAndView.addObject("goods", goodsService.getGoods());
        return modelAndView;
    }

    @ModelAttribute("addedItems")
    public List<Good> addedItems() {
        return new ArrayList<>();
    }
}
