package by.training.services;

import by.training.converter.GoodConverter;
import by.training.dao.GoodDao;
import by.training.dto.GoodDto;
import by.training.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Goods service class.
 *
 * @author m_bondarev
 * @version 1.0
 */

@Service
public class GoodsService {
    private static final Logger LOGGER = Logger.getLogger(GoodsService.class.getName());

    private final GoodDao goodDao;
    private final GoodConverter goodConverter;

    public GoodsService(GoodDao goodDao, GoodConverter goodConverter) {
        this.goodDao = goodDao;
        this.goodConverter = goodConverter;
    }

    /**
     * Returns good by id.
     * @param id id
     * @return good by id
     */
    public GoodDto getGoodById(int id) {
        try {
            return goodConverter.toDto(goodDao.getGoodById(id));
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("Good not found");
    }

    /**
     * Returns list of all goods.
     * @return list of goods
     */
    public List<GoodDto> getGoods() {
        try {
            return goodDao.getGoods().stream()
                                     .map(goodConverter::toDto)
                                     .collect(Collectors.toList());
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("Good not found");
    }
}
