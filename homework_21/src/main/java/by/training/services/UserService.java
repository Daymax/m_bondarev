package by.training.services;

import by.training.converter.UserConverter;
import by.training.dao.UserDao;
import by.training.dto.UserDto;
import by.training.entities.User;
import org.springframework.stereotype.Service;

/**
 * User service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
public class UserService {
    private final UserDao userDao;
    private final UserConverter userConverter;

    public UserService(UserDao userDao, UserConverter userConverter) {
        this.userDao = userDao;
        this.userConverter = userConverter;
    }

    /**
     * Returns user by username.
     *
     * @param username username
     * @return User
     */
    public User getUserByName(String username) {
        return userDao.getUserByUsername(username);
    }

    /**
     * Adds user to data source.
     *
     * @param userDto userDto
     */
    public void addUser(UserDto userDto) {
        userDao.addUser(userConverter.toEntity(userDto));
    }
}
