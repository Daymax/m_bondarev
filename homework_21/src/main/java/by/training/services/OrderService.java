package by.training.services;

import by.training.converter.GoodConverter;
import by.training.converter.OrderConverter;
import by.training.converter.UserConverter;
import by.training.dao.OrderDao;
import by.training.dao.OrderGoodDao;
import by.training.dto.GoodDto;
import by.training.dto.OrderDto;
import by.training.entities.Good;
import by.training.entities.Order;
import by.training.entities.User;
import by.training.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Order service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
public class OrderService {
    private static final Logger LOGGER = Logger.getLogger(OrderService.class.getName());
    private final OrderDao orderDao;
    private final OrderGoodDao orderGoodDao;
    private final OrderConverter orderConverter;
    private final GoodConverter goodConverter;
    private final UserConverter userConverter;

    public OrderService(OrderDao orderDao, OrderGoodDao orderGoodDao,
                        OrderConverter orderConverter, GoodConverter goodConverter,
                        UserConverter userConverter) {
        this.orderDao = orderDao;
        this.orderGoodDao = orderGoodDao;
        this.orderConverter = orderConverter;
        this.goodConverter = goodConverter;
        this.userConverter = userConverter;
    }

    /**
     * Saves order to data source.
     *
     * @param user       user
     * @param addedGoods session added goods
     */
    public void saveOrder(User user, List<GoodDto> addedGoods) {

        int totalPrice = addedGoods.stream().mapToInt(GoodDto::getPrice).sum();
        Order order = new Order(user.getId(), totalPrice);
        int generatedOrderId = 0;
        try {
            generatedOrderId = orderDao.addOrder(order);
            order.setId(generatedOrderId);
            for (GoodDto goodDto : addedGoods) {
                orderGoodDao.addOrderGood(order, goodConverter.toEntity(goodDto));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Returns user's orders.
     *
     * @param user user
     * @return list of the orders
     */
    public List<OrderDto> getUserOrders(User user) {
        try {
            List<Order> orders = orderDao.getUserOrders(user);
            return orders.stream().map(orderConverter::toDto).collect(Collectors.toList());
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("Orders not found");
    }

    /**
     * Returns orders's goods.
     *
     * @param orderId order id
     * @return list of the goods
     */
    public List<GoodDto> getOrderGoodsById(Integer orderId) {
        try {
            List<Good> goods = orderGoodDao.getOrderGoodsById(orderId);
            return goods.stream().map(goodConverter::toDto).collect(Collectors.toList());
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("Order not found");
    }

    /**
     * Returns order by id.
     *
     * @param id id
     * @return order
     */
    public OrderDto getOrderById(Integer id) {
        try {
            return orderConverter.toDto(orderDao.getOrderById(id));
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("Order not found");
    }
}
