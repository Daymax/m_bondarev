package by.training.entities;

public class Order {
    private Integer id;
    private Integer userId;
    private Integer totalPrice;

    public Order(Integer id, Integer userId, Integer totalPrice) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public Order(Integer userId, Integer totalPrice) {
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public Order() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }
}
