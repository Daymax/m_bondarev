package by.training.dto;

/**
 * GoodDto class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class GoodDto {
    private Integer id;
    private String title;
    private Integer price;

    public GoodDto() {}

    public GoodDto(String title) {
        this.title = title;
    }

    public GoodDto(String title, Integer price) {
        this.title = title;
        this.price = price;
    }

    public GoodDto(Integer id, String title, Integer price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
