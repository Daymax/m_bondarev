package by.training.dto;

/**
 * OrderDto class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class OrderDto {
    private Integer id;
    private Integer userId;
    private Integer totalPrice;

    public OrderDto() {}

    public OrderDto(Integer userId, Integer totalPrice) {
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public OrderDto(Integer id, Integer userId, Integer totalPrice) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
