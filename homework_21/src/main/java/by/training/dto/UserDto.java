package by.training.dto;

/**
 * UserDto class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class UserDto {
    private Integer id;
    private String username;
    private String password;
    private String role;

    public UserDto() {

    }

    public UserDto(Integer id, String username) {
        this.id = id;
        this.username = username;
    }

    public UserDto(String username) {
        this.username = username;
    }

    public UserDto(Integer id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public UserDto(String username, String password, String role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public UserDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserDto(int id, String username, String password, String role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
