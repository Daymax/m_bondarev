package by.training.dao.impl;

import by.training.dao.OrderDao;
import by.training.entities.Order;
import by.training.entities.User;
import by.training.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Order DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see OrderDao
 */
@Repository
public class OrderDaoImpl implements OrderDao {
    private static final String SELECT_USER_ORDERS_SQL_STATEMENT =
            "select * from orders where user_id=?";

    private static final String SELECT_ORDER_SQL_STATEMENT =
            "select * from orders where id=?";

    private static final String INSERT_ORDER_SQL_STATEMENT =
            "insert into orders(user_id,total_price) values(?,?)";

    private final EmbeddedDatabase dataSource;

    @Autowired
    public OrderDaoImpl(EmbeddedDatabase dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Order> getUserOrders(User user) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_USER_ORDERS_SQL_STATEMENT)) {

            preparedStatement.setInt(1, user.getId());

            try (ResultSet rs = preparedStatement.executeQuery()) {
                List<Order> orders = new ArrayList<>();
                while (rs.next()) {
                    orders.add(new Order(rs.getInt("id"),
                                         rs.getInt("user_id"),
                                         rs.getInt("total_price")));
                }
                return orders;
            }
        }
    }

    @Override
    public Order getOrderById(int id) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_ORDER_SQL_STATEMENT)) {

            preparedStatement.setInt(1, id);

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    return new Order(rs.getInt("id"),
                                     rs.getInt("user_id"),
                                     rs.getInt("total_price"));
                }
                throw new NotFoundException("Order not found");
            }
        }
    }

    @Override
    public int addOrder(Order order) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(INSERT_ORDER_SQL_STATEMENT,
                             Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setInt(1, order.getUserId());
            preparedStatement.setInt(2, order.getTotalPrice());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                } else {
                    throw new SQLException("Getting generated keys exception");
                }
            }
        }
    }
}