package by.training.dao.impl;

import by.training.dao.UserDao;
import by.training.entities.User;
import by.training.exceptions.NotFoundException;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see UserDao
 */
@Component
public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class.getName());

    private static final String MERGE_USER_SQL_STATEMENT =
            "insert into users(username,password,role) values (?,?,?)";

    private static final String SELECT_USER_SQL_STATEMENT =
            "select * from users where username=?";

    private final EmbeddedDatabase dataSource;

    public UserDaoImpl(EmbeddedDatabase dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addUser(User user) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(MERGE_USER_SQL_STATEMENT)) {

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getRole());
            preparedStatement.executeUpdate();

        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
    }

    @Override
    public User getUserByUsername(String username) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_USER_SQL_STATEMENT)) {

            preparedStatement.setString(1, username);
            try (ResultSet rs = preparedStatement.executeQuery();) {
                while (rs.next()) {
                    return new User(rs.getInt("id"),
                                    rs.getString("username"),
                                    rs.getString("password"),
                                    rs.getString("role"));
                }
            }
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("User not found");
    }
}