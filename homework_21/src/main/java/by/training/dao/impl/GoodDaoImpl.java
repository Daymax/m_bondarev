package by.training.dao.impl;

import by.training.dao.GoodDao;
import by.training.entities.Good;
import by.training.exceptions.NotFoundException;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Good DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see GoodDao
 */
@Component
public class GoodDaoImpl implements GoodDao {
    private final EmbeddedDatabase dataSource;

    public GoodDaoImpl(EmbeddedDatabase dataSource) {
        this.dataSource = dataSource;
    }

    private static final String SELECT_ALL_GOODS_SQL_STATEMENT =
            "select * from goods";

    private static final String SELECT_GOOD_SQL_STATEMENT =
            "select * from goods where id=?";

    @Override
    public Good getGoodById(int id) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_GOOD_SQL_STATEMENT)) {
            preparedStatement.setInt(1, id);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    return new Good(rs.getInt("id"),
                                    rs.getString("title"),
                                    rs.getInt("price"));
                }
            }
        }
        throw new NotFoundException("Good not found");
    }

    @Override
    public List<Good> getGoods() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_ALL_GOODS_SQL_STATEMENT);
             ResultSet rs = preparedStatement.executeQuery()) {
            List<Good> goods = new ArrayList<>();
            while (rs.next()) {
                goods.add(new Good(rs.getInt("id"),
                                   rs.getString("title"),
                                   rs.getInt("price")));
            }
            return goods;
        }
    }
}
