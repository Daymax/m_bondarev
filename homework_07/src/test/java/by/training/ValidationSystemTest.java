package by.training;

import org.junit.Test;

/**
 * ValidationSystemTest utility class.
 */

public class ValidationSystemTest {

    /**
     * Method testValidateInt for verify validation integer data.
     * @throws ValidationFailedException when object isn't valid.
     */

    @Test
    public void testValidateInteger () throws ValidationFailedException {
        ValidationSystem.validate(1);
        ValidationSystem.validate(5);
        ValidationSystem.validate(10);
    }

    /**
     * Method testValidateIntFails for catch ValidationFailedException when input data wasn't included in the interval from 1 to 10.
     * @throws ValidationFailedException when object isn't valid.
     */

    @Test (expected = ValidationFailedException.class)
    public void testValidateIntegerWithValidateFailedExceptionFirst() throws ValidationFailedException {
        ValidationSystem.validate(11);
    }

    /**
     * Method testValidateIntFails2 for catch ValidationFailedException when input data wasn't included in the interval from 1 to 10.
     * @throws ValidationFailedException when object isn't valid
     */

    @Test (expected = ValidationFailedException.class)
    public void testValidateIntegerWithValidateFailedExceptionSecond() throws ValidationFailedException {
        ValidationSystem.validate(0);
    }

    /**
     * Method testValidateString for verify validation string data.
     * @throws ValidationFailedException when object isn't valid
     */

    @Test
    public void testValidateString () throws ValidationFailedException {
        ValidationSystem.validate("Hello");
        ValidationSystem.validate("Hello world, abc");
    }

    /**
     * Method testValidateStringFails for catch validationFailedExceptions when data isn't begin with upper case.
     * @throws ValidationFailedException when object isn't valid
     */

    @Test (expected = ValidationFailedException.class)
    public void testValidateStringWithValidationFailedExceptionFirst() throws ValidationFailedException {
        ValidationSystem.validate("hello");
    }

    /**
     * Method testValidateStringFails2 for catch validationFailedExceptions when data be away.
     * @throws ValidationFailedException when object isn't valid
     */

    @Test (expected = ValidationFailedException.class)
    public void testValidateStringFailsSecond() throws ValidationFailedException {
        ValidationSystem.validate("");
    }

}