package by.training.validators;

import by.training.ValidationFailedException;

/**
 * IntegerValidator utility class.
 * @author m_bondarev
 * @version 1.0
 */

public class IntegerValidator implements Validator {

    /**
     * Method for validate Integer object which must be in range 1 to 10.
     * @param object                      input object
     * @throws ValidationFailedException when object isn't valid
     */

    @Override
    public void validate(Object object) throws ValidationFailedException {

        if ((int)object >= 1 && (int)object <= 10) {
            System.out.println("Validate!");
        } else  {
            throw new ValidationFailedException("Validation failed");
        }
    }
}
