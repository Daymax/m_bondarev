package by.training.validators;

import by.training.ValidationFailedException;

/**
 * StringValidator utility class.
 * @author m_bondarev
 * @version 1.0
 */

public class StringValidator implements Validator {

    /**
     * Method for validate String object which must be only with upper case.
     * @param object                      input object
     * @throws ValidationFailedException when object isn't valid
     */

    @Override
    public void validate(Object object) throws ValidationFailedException {
        String regex = "^[A-Z].*$";
        if (object.toString().matches(regex)) {
            System.out.println("Validate!");
        } else {
            throw new ValidationFailedException("Validation failed");
        }
    }
}

