package by.training.validators;

import by.training.ValidationFailedException;

/**
 * Interface validator.
 */

public interface Validator<T> {
    void validate(T object) throws ValidationFailedException;
}
