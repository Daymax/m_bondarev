package by.training;

/**
 * ValidationFailedException utility class.
 * @author m_bondarev
 * @version 1.0
 */

public class ValidationFailedException extends Exception {
    public ValidationFailedException(String message) {
        super(message);
    }
}
