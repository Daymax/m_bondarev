package by.training;

import by.training.validators.IntegerValidator;
import by.training.validators.StringValidator;
import by.training.validators.Validator;

import java.util.HashMap;
import java.util.Map;

/**
 * Validation class.
 * @author m_bondarev
 * @version 1.0
 * @see Validator
 */

public class ValidationSystem {
    private static Map<String, Validator> validators = new HashMap<>();

    static {
        registerValidator(Integer.class.getName(), new IntegerValidator());
        registerValidator(String.class.getName(), new StringValidator());
    }

    /**
     * Method for choose validator and validate object.
     * @param object                      input object
     * @throws ValidationFailedException when object isn't valid
     */

    public static <T> void validate(T object) throws ValidationFailedException {
        Validator<T> validator = validators.get(object.getClass().getCanonicalName());
        try {
            validator.validate(object);
        } catch (NullPointerException exception) {
            throw new ValidationFailedException("Validation is wrong!");
        }
    }

    /**
     * Method for register the Validator.
     * @param validatorClassName className
     * @param validator          validator
     */

    private static void registerValidator(String validatorClassName, Validator validator) {
        validators.put(validatorClassName, validator);
    }
}
