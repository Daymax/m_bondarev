<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Online shop</title>
    <style>
        .list-group-item {
            display: list-item;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="${pageContext.request.contextPath}">Online-shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}">Shop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="order">My orders</a>
            </li>
        </ul>
        <form action="logout" method="post" class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
        </form>
    </div>
</nav>
<div class="container">
    <div class="row justify-content-center" style="margin: 20px">
        <h1>Dear ${user.name}, your order:</h1>
    </div>
    <div class="row justify-content-center">
        <div class="col-8">
            <c:if test="${empty sessionScope.addedItems}">
                <p class="h5">You shopping cart is empty!</p>
            </c:if>
            <c:if test="${!empty sessionScope.addedItems}">
                <ol class="item-steps list-group">
                    <c:forEach var="item" items="${sessionScope.addedItems}">
                        <li class="list-group-item">${item.title} ${item.price}$</li>
                    </c:forEach>
                </ol>
                <c:set var="total" value="${0}"/>
                <c:forEach var="item" items="${sessionScope.addedItems}">
                    <c:set var="total" value="${total + item.price}"/>
                </c:forEach>
                <p class="h5" style="margin-top: 20px">Total: ${total}$</p>
                <form action="order" method="post" style="margin: 20px">
                    <button type="submit" class="btn btn-primary">Make order</button>
                </form>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
