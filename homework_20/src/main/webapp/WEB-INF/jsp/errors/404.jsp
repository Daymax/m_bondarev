<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Online shop</title>
</head>
<body>

<div class="container">
    <div class="row justify-content-center" style="margin: 20px">
        <img src="images/404.png"/>
    </div>
    <div class="row justify-content-center">
        <h3>The URL you requested was not found.</h3>
    </div>
    <div class="row justify-content-center">
        <form action="/online-shop" method="post">
            <button type="submit" class="btn btn-primary">Main page</button>
        </form>
    </div>
</div>
</body>
</html>
