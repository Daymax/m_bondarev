package by.training.entities;

public class Good {
    private int id;
    private String title;
    private int price;

    public Good() {
    }

    public Good(int id, String title, int price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public Good(String title, int price) {
        this.title = title;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
