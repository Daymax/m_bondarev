package by.training.dao;

import by.training.entities.Good;
import by.training.entities.Order;

import java.sql.SQLException;
import java.util.List;

/**
 * Order_Good DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface OrderGoodDao {

    /**
     * Adds order's good to data source.
     * @param order order
     * @param good good
     */
    void addOrderGood(Order order, Good good) throws SQLException;

    /**
     * Returns list of goods of the order by id.
     * @param orderId order's id
     */
    List<Good> getOrderGoodsById(int orderId) throws SQLException;
}

