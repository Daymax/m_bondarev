package by.training.dao.impl;

import by.training.dao.OrderGoodDao;
import by.training.entities.Good;
import by.training.entities.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * OrderGood DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see OrderGoodDao
 */
@Repository
public class OrderGoodDaoImpl implements OrderGoodDao {
    private static final String INSERT_ORDER_SQL_STATEMENT =
            "insert into order_good(order_id,good_id) values(?,?)";

    private static final String SELECT_ORDER_GOODS_SQL_STATEMENT =
            "select g.* from goods as g join order_good as o_g "
                    + "on g.id = o_g.good_id where o_g.order_id=?";

    private final EmbeddedDatabase dataSource;

    @Autowired
    public OrderGoodDaoImpl(EmbeddedDatabase dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addOrderGood(Order order, Good good) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(INSERT_ORDER_SQL_STATEMENT)) {

            preparedStatement.setInt(1, order.getId());
            preparedStatement.setInt(2, good.getId());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public List<Good> getOrderGoodsById(int orderId) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_ORDER_GOODS_SQL_STATEMENT)) {

            preparedStatement.setInt(1, orderId);

            try (ResultSet rs = preparedStatement.executeQuery()) {
                List<Good> goods = new ArrayList<>();
                while (rs.next()) {
                    goods.add(new Good(rs.getInt("id"),
                                       rs.getString("title"),
                                       rs.getInt("price")));
                }
                return goods;
            }
        }
    }
}
