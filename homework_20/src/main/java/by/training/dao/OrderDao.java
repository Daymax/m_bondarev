package by.training.dao;

import by.training.entities.Order;
import by.training.entities.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Order DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface OrderDao {
    /**
     * Returns order by id.
     * @param id id
     * @return order by id
     */
    Order getOrderById(int id) throws SQLException;

    /**
     * Returns user's orders.
     * @param user user
     * @return user's orders
     */
    List<Order> getUserOrders(User user) throws SQLException;

    /**
     * Adds order to data source.
     * @param order order
     * @return generated order's id
     */
    int addOrder(Order order) throws SQLException;
}
