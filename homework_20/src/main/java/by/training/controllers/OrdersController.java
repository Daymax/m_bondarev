package by.training.controllers;

import by.training.entities.Good;
import by.training.entities.Order;
import by.training.entities.User;
import by.training.services.OrderService;
import by.training.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class OrdersController {
    private final OrderService orderService;
    private final UserService userService;

    public OrdersController(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    @GetMapping("/order")
    public String getOrders(Principal principalUser,
                            Model model) {
        User user = userService.getUserByName(principalUser.getName());
        model.addAttribute("orders", orderService.getUserOrders(user));
        return "orders";
    }

    @PostMapping("/order")
    public String saveOrder(Principal principalUser,
                            Model model,
                            HttpServletRequest request) {
        User user = userService.getUserByName(principalUser.getName());
        List<Good> addedItems = (List) request.getSession().getAttribute("addedItems");
        orderService.saveOrder(user, addedItems);
        request.getSession().setAttribute("addedItems", new ArrayList<>());
        model.addAttribute("orders", orderService.getUserOrders(user));
        return "orders";
    }

    @RequestMapping("/order/{orderId}")
    public String getOrder(@PathVariable("orderId") int orderId,
                           Model model) {
        Optional<Order> order = Optional.ofNullable(orderService.getOrderById(orderId));
        if (order.isPresent()) {
            model.addAttribute("order", order.get());
            model.addAttribute("goods", orderService.getOrderGoodsById(orderId));
            return "order";
        } else {
            return "errors/404";
        }
    }
}
