package by.training.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class CheckoutController {
    @RequestMapping(value = "/checkout")
    public String postShopPage(Principal user, Model model) {
        model.addAttribute("user", user);
        return "checkout";
    }
}
