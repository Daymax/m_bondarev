package by.training.services;

import by.training.dao.GoodDao;
import by.training.dao.impl.GoodDaoImpl;
import by.training.entities.Good;
import by.training.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Goods service class.
 *
 * @author m_bondarev
 * @version 1.0
 */

@Service
public class GoodsService {
    private static final Logger LOGGER = Logger.getLogger(GoodsService.class.getName());

    private final GoodDao goodDao;

    public GoodsService(GoodDaoImpl goodDao) {
        this.goodDao = goodDao;
    }

    /**
     * Returns good by id.
     * @param id id
     * @return good by id
     */
    public Good getGoodById(int id) {
        try {
            return goodDao.getGoodById(id);
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("Good not found");
    }

    /**
     * Returns list of all goods.
     * @return list of goods
     */
    public List<Good> getGoods() {
        try {
            return goodDao.getGoods();
        } catch (SQLException exception) {
            LOGGER.log(Level.SEVERE, exception.getMessage());
        }
        throw new NotFoundException("Good not found");
    }
}
