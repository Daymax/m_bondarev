package by.training.services;

import by.training.dao.UserDao;
import by.training.entities.User;
import org.springframework.stereotype.Service;

/**
 * User service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
public class UserService {
    private final UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }


    /**
     * Returns user by username.
     *
     * @param username username
     * @return User
     */
    public User getUserByName(String username) {
        return userDao.getUserByUsername(username);
    }

    /**
     * Adds user to data source.
     *
     * @param user user
     */
    public void addUser(User user) {
        userDao.addUser(user);
    }
}
