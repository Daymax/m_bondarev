# How to run

  Run command:
```sh
mvn tomcat7:run
```
Then go to:
```sh
localhost:8080/online-shop
```
You can login with:


| login        | password| 
| ------------- |:-------:|
| user      | user    |
| admin      | admin   |