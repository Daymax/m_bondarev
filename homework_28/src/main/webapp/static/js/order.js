Vue.component(
    'goods', {
        data: function () {
            return {
                orderId: null,
                order: {}
            }
        },
        template:
            '<div style="margin-top: 20px">' +
            '<h1>Order #{{orderId}}</h1>' +
            '<table class="table table-striped">' +
            '                <thead>' +
            '                <tr>' +
            '                    <th scope="col">#</th>' +
            '                    <th scope="col">Title</th>' +
            '                    <th scope="col">Price</th>' +
            '                </tr>' +
            '                </thead>' +
            '                <tbody>' +
            '                <tr v-for="(good,index) in order.goods">' +
            '                    <th scope="row">{{index+1}}</th>' +
            '                    <th>{{good.title}}</th>' +
            '                    <td >{{good.price}}</td>' +
            '                </tr>' +
            '                </tbody>' +
            '            </table>' +
            '            <p class="h5">Total price: {{order.totalPrice}}$</p> ' +
            '</div>',
        mounted() {
            this.orderId = window.location.href.split('/').pop();
            axios
                .get('http://localhost:8080/online-shop/api/orders/' + this.orderId)
                .then(response => this.order = response.data)
                .catch(error => window.location.href = 'http://localhost:8080/online-shop/errors/404');
        }
    }
);

var app = new Vue({
    el: '#order',
    template: '<goods/>'
});

