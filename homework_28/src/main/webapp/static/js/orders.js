Vue.component(
    'orders', {
        props: ['orders'],
        template:
            '<div v-if="orders.length === 0" style="margin-top: 20px">' +
            '<p class="h5">You orders list is empty!</p>' +
            '</div>' +
            '<div v-else style="margin-top: 20px">' +
            '<p class="h5">Your orders:</p>' +
            '<table class="table table-striped">' +
            '                <thead>' +
            '                <tr>' +
            '                    <th scope="col">#</th>' +
            '                    <th scope="col">Total price</th>' +
            '                    <th scope="col"></th>' +
            '                </tr>' +
            '                </thead>' +
            '                <tbody>' +
            '                <tr v-for="(order,index) in orders">' +
            '                    <th scope="row">{{order.id}}</th>' +
            '                    <td >{{order.totalPrice}}</td>' +
            '                    <td style="width: 20%">' +
            '                            <button @click="forward(order.id)" ' +
            '                                    class="btn btn-outline-danger my-2 my-sm-0">Show details' +
            '                            </button>' +
            '                    </td>' +
            '                </tr>' +
            '                </tbody>' +
            '            </table>' +
            '</div>',
        methods: {
            forward: function (orderId) {
                window.location.href = 'http://localhost:8080/online-shop/orders/'+(orderId);
            }
        }
    }
);

var app = new Vue({
    el: '#orders',
    template: '<orders :orders="orders"></orders>',
    data: {
        orders: [],
    },
    mounted() {
        axios
            .get('http://localhost:8080/online-shop/api/orders')
            .then(response => (this.orders = response.data));
    }
});

