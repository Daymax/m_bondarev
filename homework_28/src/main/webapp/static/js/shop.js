Vue.component('goods-form', {
    props: ['cart'],
    data: function () {
        return {
            selected: 1,
            goods: null
        }
    },
    template: '<div class="input-group"><select v-model="selected" class="custom-select">' +
        '<option v-for="good in goods" v-bind:value="good.id">{{good.title}} - {{good.price}}$</option>' +
        '</select>' +
        '<div class="input-group-append"><button class="btn btn-success" @click="addItem()">Add Item</button></div><br>' +
        '</div>',
    methods: {
        addItem: function () {
            axios
                .post('http://localhost:8080/online-shop/api/carts', {id: this.selected})
                .then(response => (this.cart.push(response.data)));
        }
    },
    mounted() {
        axios
            .get('http://localhost:8080/online-shop/api/goods')
            .then(response => (this.goods = response.data));
    }
});

Vue.component(
    'cart-table', {
        props: ['cart'],
        template:
            '<div v-if="cart.length === 0" style="margin-top: 20px">' +
            '<p class="h5">You shopping cart is empty!</p>' +
            '</div>' +
            '<div v-else style="margin-top: 20px">' +
            '<p class="h5">You have already chosen:</p>' +
            '<table class="table table-striped">' +
            '                <thead>' +
            '                <tr>' +
            '                    <th scope="col">#</th>' +
            '                    <th scope="col">Name</th>' +
            '                    <th scope="col">Price</th>' +
            '                    <th scope="col"></th>' +
            '                </tr>' +
            '                </thead>' +
            '                <tbody>' +
            '                <tr v-for="(good,index) in cart">' +
            '                    <th scope="row">{{index+1}}</th>' +
            '                    <td >{{good.title}}</td>' +
            '                    <td >{{good.price}}</td>' +
            '                    <td style="width: 20%">' +
            '                            <button @click="deleteItem(index)" class="btn btn-outline-danger my-2 my-sm-0">Delete item' +
            '                            </button>' +
            '                    </td>' +
            '                </tr>' +
            '                </tbody>' +
            '            </table>' +
            '<button  @click="makeOrder()" type="button">\n' +
            '         Make order</button>' +
            '</div>',
        methods: {
            deleteItem: function (buttonItemId) {
                axios
                    .delete('http://localhost:8080/online-shop/api/carts/' + (buttonItemId + 1));
                this.cart.splice(buttonItemId,1);
            },
            makeOrder: function () {
                axios
                    .post('http://localhost:8080/online-shop/api/orders/', this.cart.map(item => item.id))
                    .then(window.location.href = "http://localhost:8080/online-shop/orders/");
            }
        }
    }
);

var app = new Vue({
    el: '#goods',
    template: '<div><goods-form :cart="cart"></goods-form><cart-table :cart="cart"></cart-table></div>',
    data: {
        cart: [],
    },
    mounted() {
        axios
            .get('http://localhost:8080/online-shop/api/carts')
            .then(response => (this.cart = response.data));
    }
});

