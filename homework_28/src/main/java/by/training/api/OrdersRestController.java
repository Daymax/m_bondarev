package by.training.api;

import by.training.converters.Converter;
import by.training.dto.GoodDto;
import by.training.dto.OrderDto;
import by.training.entities.Order;
import by.training.exceptions.OrderNotFoundException;
import by.training.exceptions.UserNotFoundException;
import by.training.services.OrdersService;
import by.training.services.UserService;
import by.training.utils.MailSender;
import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

/**
 * Order rest controller.
 * Mapping "/api/orders/"
 *
 * @author m_bondarev
 * @version 1.0
 */
@RestController
@RequestMapping(value = "/api/orders")
public class OrdersRestController {

    private final OrdersService ordersService;
    private final UserService userService;
    private final Converter converter;
    private final MailSender mailSender;


    public OrdersRestController(OrdersService ordersService,
                                UserService userService,
                                Converter converter,
                                MailSender mailSender) {
        this.ordersService = ordersService;
        this.userService = userService;
        this.converter = converter;
        this.mailSender = mailSender;
    }

    @GetMapping
    public ResponseEntity<List<OrderDto>> getOrders(Principal principal){
        return ResponseEntity.ok(
                ordersService.findAllByUserId(
                        userService.getUserByName(principal.getName()).orElseThrow(UserNotFoundException::new).getId()));
    }

    @GetMapping(path = "/{order_id}")
    public ResponseEntity<Order> getOrderById(@PathVariable Long order_id)
            throws OrderNotFoundException {
        return ResponseEntity.ok(ordersService.findById(order_id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void saveOrderWithGoods(@RequestBody List<GoodDto> goodsDTO,
                                   Principal principal,
                                   HttpServletRequest request) throws MessagingException {
        request.getSession().setAttribute("sessionGoods", Lists.newArrayList());
        Order order = ordersService.save(
                userService.getUserByName(principal.getName())
                           .orElseThrow(UserNotFoundException::new)
                           .getId(), converter.toListGood(goodsDTO));
        mailSender.sendOrderDetails(order);
    }

    @ExceptionHandler(OrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity handleOrderNotFoundException() {
        return ResponseEntity.notFound().build();
    }
}
