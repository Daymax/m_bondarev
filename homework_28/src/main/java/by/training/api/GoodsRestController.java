package by.training.api;

import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;
import by.training.services.GoodsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/goods")
public class GoodsRestController {

    private GoodsService goodsService;

    public GoodsRestController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @GetMapping
    public ResponseEntity<List<Good>> getAllGoods() {
        return ResponseEntity.ok(goodsService.findAll());
    }

    @GetMapping(path = "/{good_id}")
    public ResponseEntity<Good> getGoodById(@PathVariable Long good_id)
            throws GoodNotFoundException {
        return ResponseEntity.ok(goodsService.findById(good_id));
    }

    @ExceptionHandler(GoodNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity handleGoodNotFoundException() {
        return ResponseEntity.notFound().build();
    }
}
