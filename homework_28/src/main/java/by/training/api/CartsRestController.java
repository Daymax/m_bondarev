package by.training.api;

import by.training.dto.GoodDto;
import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;
import by.training.services.GoodsService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/carts")
public class CartsRestController {

    @Autowired
    private GoodsService goodsService;

    @GetMapping
    public ResponseEntity<List<Good>> getGoods(HttpServletRequest request) {
        List<Good> sessionGoods = (List) request.getSession().getAttribute("sessionGoods");
        if (sessionGoods == null) {
            return ResponseEntity.ok(Lists.newArrayList());
        }
        return ResponseEntity.ok(sessionGoods);
    }

    @PostMapping
    public ResponseEntity<Good> saveSelectedGood(@RequestBody GoodDto goodDTO, HttpServletRequest request) throws GoodNotFoundException {
        Good good = goodsService.findById(goodDTO.getId());
        List<Good> sessionGoods = (List) request.getSession().getAttribute("sessionGoods");
        if (sessionGoods == null) {
            sessionGoods = Lists.newArrayList();
        }
        sessionGoods.add(good);
        request.getSession().setAttribute("sessionGoods", sessionGoods);
        return ResponseEntity.status(HttpStatus.CREATED).body(good);
    }

    @DeleteMapping("/{deletedId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Integer deletedId, HttpServletRequest request) {
        List<Good> sessionGoods = (List) request.getSession().getAttribute("sessionGoods");
        if (sessionGoods != null) {
            sessionGoods.remove(deletedId - 1);
            request.getSession().setAttribute("sessionGoods", sessionGoods);
        }
    }

}
