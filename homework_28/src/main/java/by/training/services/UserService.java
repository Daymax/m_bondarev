package by.training.services;

import by.training.entities.User;

import java.util.Optional;

/**
 * User service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface UserService {
    /**
     * Returns user by username.
     *
     * @param username username
     * @return User
     */
    Optional<User> getUserByName(String username);
}
