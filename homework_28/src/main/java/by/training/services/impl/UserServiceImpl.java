package by.training.services.impl;

import by.training.dao.UserDao;
import by.training.entities.User;
import by.training.services.UserService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * User service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public Optional<User> getUserByName(String username) {
        return userDao.getUserByUsername(username);
    }
}
