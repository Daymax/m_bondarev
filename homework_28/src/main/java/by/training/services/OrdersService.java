package by.training.services;

import by.training.dto.OrderDto;
import by.training.entities.Good;
import by.training.entities.Order;
import by.training.exceptions.OrderNotFoundException;

import java.util.List;

/**
 * Order service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface OrdersService {
    /**
     * Saves order to data source.
     *
     * @param userId     user id
     * @param addedGoods session added goods
     */
    Order save(Long userId, List<Good> addedGoods);

    /**
     * Returns user's orders.
     *
     * @param userId user id
     * @return list of the orders
     */
    List<OrderDto> findAllByUserId(Long userId);

    /**
     * Returns order by id.
     *
     * @param id id
     * @return order
     */
    Order findById(Long id) throws OrderNotFoundException;
}
