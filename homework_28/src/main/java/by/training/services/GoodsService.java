package by.training.services;

import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;

import java.util.List;

/**
 * Goods service class.
 *
 * @author m_bondarev
 * @version 1.0
 */

public interface GoodsService {
    /**
     * Returns good by id.
     *
     * @param id id
     * @return good by id
     */
    Good findById(Long id) throws GoodNotFoundException;

    /**
     * Returns list of all goods.
     *
     * @return list of goods
     */
    List<Good> findAll();
}
