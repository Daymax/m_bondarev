package by.training.converters;

import by.training.dto.GoodDto;
import by.training.dto.OrderDto;
import by.training.entities.Good;
import by.training.entities.Order;
import by.training.exceptions.GoodNotFoundException;
import by.training.services.GoodsService;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Converter {
    private final GoodsService goodsService;

    public Converter(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    public OrderDto toOrderDTO(Order order) {
        return new OrderDto(order.getId(),order.getTotalPrice());
    }

    public List<Good> toListGood(List<GoodDto> goodDtoList) {
        List<Good> goods = Lists.newArrayList();
        goodDtoList.forEach(dto -> {
            try {
                goods.add(goodsService.findById(dto.getId()));
            } catch (GoodNotFoundException e) {
                e.printStackTrace();
            }
        });
        return goods;
    }
}
