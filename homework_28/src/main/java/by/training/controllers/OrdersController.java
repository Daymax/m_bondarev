package by.training.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OrdersController {
    @GetMapping("/orders")
    public String getOrders() {
        return "orders";
    }

    @RequestMapping("/orders/{order_id}")
    public String getOrder() {
        return "order";
    }
}
