package by.training.dto;

/**
 * GoodDto class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class GoodDto {
    private Long id;

    public GoodDto() {
    }

    public GoodDto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
