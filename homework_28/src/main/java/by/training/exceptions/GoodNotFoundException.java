package by.training.exceptions;

public class GoodNotFoundException extends Exception {
    public GoodNotFoundException() {
    }

    public GoodNotFoundException(String message) {
        super(message);
    }


}
