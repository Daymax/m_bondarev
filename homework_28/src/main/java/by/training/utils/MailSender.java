package by.training.utils;

import by.training.entities.Order;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
@PropertySource("classpath:mail.properties")
public class MailSender {
    private final JavaMailSender javaMailSender;
    private final SpringTemplateEngine templateEngine;

    public MailSender(JavaMailSender javaMailSender,
                      SpringTemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }

    @Value("${mail.username}")
    private String emailSender;

    @Value("${mail.consumer}")
    private String emailConsumer;

    public void sendOrderDetails(Order order)
            throws MessagingException {
        final Context ctx = new Context();
        ctx.setVariable("orderId", order.getId());
        ctx.setVariable("totalPrice", order.getTotalPrice());
        ctx.setVariable("goods", order.getGoods());

        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        final MimeMessageHelper message =
                new MimeMessageHelper(mimeMessage, true, "UTF-8");
        message.setFrom(emailSender);
        message.setTo(emailConsumer);
        message.setSubject("Order details");

        final String htmlContent =
                templateEngine.process("emails/orderEmail", ctx);
        message.setText(htmlContent, true);

        javaMailSender.send(mimeMessage);
    }
}
