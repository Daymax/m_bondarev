package by.training.services.impl;

import by.training.converters.Converter;
import by.training.dao.OrderDao;
import by.training.dto.OrderDto;
import by.training.entities.Good;
import by.training.entities.Order;
import by.training.exceptions.OrderNotFoundException;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceImplTest {

    @Mock
    private OrderDao orderDao;

    @Mock
    private Converter converter;

    @InjectMocks
    private OrdersServiceImpl ordersService;


    @Test
    public void testSave_with_saving_order_successfully() {
//given
        Long userId = 1L;
        List<Good> addedGoods = Lists.newArrayList();
        addedGoods.add(new Good("some title", 10));
        Optional<Order> maybeExistingOrder = Optional.of(new Order(addedGoods));

        when(orderDao.save(userId, addedGoods)).thenReturn(maybeExistingOrder);

//when
        Optional<Order> maybeOrder = Optional.of(ordersService.save(userId, addedGoods));

//then
        assertTrue(maybeOrder.isPresent());
        assertEquals(maybeExistingOrder.get(), maybeOrder.get());

        verify(orderDao).save(userId, addedGoods);
    }

    @Test(expected = OrderNotFoundException.class)
    public void testSave_with_throwing_orderNotFoundException() {
//given
        Long userId = 1L;
        List<Good> addedGoods = Lists.newArrayList();

//when
        ordersService.save(userId, addedGoods);
    }

    @Test
    public void testFindAllByUserId() {
//given
        Long userId = 1L;
        List<Good> goods = ImmutableList.<Good>builder()
                .add(new Good("some title", 10))
                .add(new Good("some title", 20))
                .add(new Good("some title", 30))
                .build();

        Integer totalPrice = 60;

        Order order = new Order(totalPrice, goods);
        OrderDto orderDto = new OrderDto(userId, totalPrice);

        Set<Order> orders = ImmutableSet.<Order>builder()
                .add(order)
                .build();

        when(orderDao.findAllByUserId(userId)).thenReturn(orders);
        when(converter.toOrderDTO(order)).thenReturn(orderDto);
//when
        List<OrderDto> foundOrdersByUserId = ordersService.findAllByUserId(userId);

//then
        assertFalse(foundOrdersByUserId.isEmpty());
        assertEquals(userId, foundOrdersByUserId.get(0).getId());

        verify(orderDao).findAllByUserId(userId);
        verify(converter).toOrderDTO(order);
    }

    @Test
    public void testFindById_with_existing_order() {
//given
        Long userId = 1L;
        List<Good> addedGoods = Lists.newArrayList();
        addedGoods.add(new Good("some title", 10));
        Optional<Order> maybeExisting = Optional.of(new Order(addedGoods));

        when(orderDao.findById(userId)).thenReturn(maybeExisting);

//when
        Optional<Order> maybeOrder = Optional.of(ordersService.findById(userId));

//then
        assertTrue(maybeOrder.isPresent());
        assertEquals(maybeExisting.get(), maybeOrder.get());

        verify(orderDao).findById(userId);
    }

    @Test(expected = OrderNotFoundException.class)
    public void testFindById_with_throwing_orderNotFoundException() {
//given
        Long userId = 1L;

//when
        ordersService.findById(userId);
    }
}