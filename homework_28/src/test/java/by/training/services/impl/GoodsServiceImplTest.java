package by.training.services.impl;

import by.training.dao.GoodDao;
import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GoodsServiceImplTest {

    @Mock
    private GoodDao goodDao;

    @InjectMocks
    private GoodsServiceImpl goodsService;

    @Test
    public void testFindById_with_existing_good() throws GoodNotFoundException {
//given
        Long id = 1L;
        Good good = new Good("some title", 10);
        good.setId(id);
        Optional<Good> optionalGood = Optional.of(good);

        when(goodDao.findById(id)).thenReturn(optionalGood);

//when
        Optional<Good> goodResponse = Optional.of(goodsService.findById(id));

//then
        assertTrue(goodResponse.isPresent());
        assertEquals(goodResponse.get().getId(), id);

        verify(goodDao).findById(id);
    }

    @Test(expected = GoodNotFoundException.class)
    public void testFindById_with_GoodNotFoundException() throws GoodNotFoundException {
//given
        Long id = 1L;

//when
        goodsService.findById(id);
    }

    @Test
    public void testFindAll_with_existing_list_with_goods() {
//given
        String title = "some title";
        ArrayList<Good> goods = Lists.newArrayList();
        goods.add(new Good(title, 10));

        when(goodDao.findAll()).thenReturn(goods);

//when
        List<Good> goodResponse = goodsService.findAll();

//then
        assertEquals(goodResponse.get(0).getTitle(), title);

        verify(goodDao).findAll();
    }
}