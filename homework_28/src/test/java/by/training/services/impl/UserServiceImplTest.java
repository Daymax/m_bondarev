package by.training.services.impl;

import by.training.dao.UserDao;
import by.training.entities.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void testGetUserByName() {

//given
        String username = "Vasya";
        User user = new User(username);
        Optional<User> maybeUser = Optional.of(user);

        when(userDao.getUserByUsername(username)).thenReturn(maybeUser);

//when
        Optional<User> maybeUserByUsername = userService.getUserByName(username);

//then
        assertTrue(maybeUserByUsername.isPresent());
        assertEquals(username, maybeUserByUsername.get().getUsername());

        verify(userDao).getUserByUsername(username);
    }

    @Test
    public void testGetUserByName_if_user_does_not_exist() {
//given
        String username = "Vasya";
        User user = new User(username);

//when
        Optional<User> maybeUserByUsername = userService.getUserByName(username);

//then
        assertFalse(maybeUserByUsername.isPresent());
    }
}