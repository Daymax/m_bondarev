package by.training.api;

import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;
import by.training.services.GoodsService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GoodsRestControllerTest {

    private MockMvc mockMvc;

    @Mock
    private GoodsService goodsService;

    @InjectMocks
    private GoodsRestController goodsRestController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(goodsRestController).build();
    }

    @Test
    public void testGetAllGoods_with_existing_goods() throws Exception {

//given
        Good good = new Good("some title", 10);
        good.setId(1L);
        List<Good> goods = ImmutableList.<Good>builder()
                .add(good)
                .build();

        Gson gson = new Gson();

        when(goodsService.findAll()).thenReturn(goods);

//when
        MvcResult result = this.mockMvc.perform(get("/api/goods"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().encoding("UTF-8"))
                .andReturn();
//then
        assertEquals(gson.toJson(goods), result.getResponse().getContentAsString());
    }

    @Test
    public void testGetGoodById_with_existing_good() throws Exception {
//given
        Long id = 1L;
        Good good = new Good("some title", 10);
        good.setId(id);

        Gson gson = new Gson();

        when(goodsService.findById(id)).thenReturn(good);

//when
        MvcResult result = this.mockMvc.perform(get("/api/goods/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().encoding("UTF-8"))
                .andReturn();
//then
        assertEquals(gson.toJson(good), result.getResponse().getContentAsString());
    }
}