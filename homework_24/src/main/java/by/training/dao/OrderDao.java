package by.training.dao;


import by.training.entities.Good;
import by.training.entities.Order;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Order DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface OrderDao {
    /**
     * Returns order by id.
     *
     * @param id id
     * @return order by id
     */
    Optional<Order> findById(Long id);

    /**
     * Returns user's orders.
     *
     * @param userId user id
     * @return user's orders
     */
    Set<Order> findAllByUserId(Long userId);

    /**
     * Adds order to data source.
     *
     * @param goods goods
     */
    void save(Long userId, List<Good> goods);

    /**
     * Adds order to data source.
     *
     * @param id order id
     */
    void remove(Long id);
}
