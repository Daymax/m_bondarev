package by.training.dao;


import by.training.entities.User;

import java.util.Optional;

/**
 * User DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface UserDao {
    /**
     * Returns user by username.
     *
     * @param username username
     */
    Optional<User> getUserByUsername(String username);
}
