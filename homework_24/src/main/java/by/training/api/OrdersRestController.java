package by.training.api;

import by.training.converters.Converter;
import by.training.dto.GoodDto;
import by.training.dto.OrderDto;
import by.training.entities.Order;
import by.training.entities.User;
import by.training.exceptions.OrderNotFoundException;
import by.training.services.OrdersService;
import by.training.services.UserService;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Api(value = "Orders Controller", tags = {"Orders Controller"})
@RestController
@RequestMapping(value = "/api/orders")
public class OrdersRestController {

    private final OrdersService ordersService;
    private final UserService userService;
    private final Converter converter;

    public OrdersRestController(OrdersService ordersService, UserService userService, Converter converter) {
        this.ordersService = ordersService;
        this.userService = userService;
        this.converter = converter;
    }

    @ApiOperation(value = "Get all orders")
    @GetMapping
    public ResponseEntity<List<OrderDto>> get(Principal principal){
        User user = userService.getUserByName(principal.getName()).get();

        return ResponseEntity.ok(ordersService.findAllByUserId(user.getId()));
    }

    @ApiOperation(value = "Get order by id")
    @GetMapping(path = "/{order_id}")
    public ResponseEntity<Order> get(@PathVariable Long order_id)
            throws OrderNotFoundException {
        return ResponseEntity.ok(ordersService.findById(order_id));
    }

    @ApiOperation(value = "Add good to order")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void post(@RequestBody List<GoodDto> goodsDTO,
                     Principal principal,
                     HttpServletRequest request)  {
        User user = userService.getUserByName(principal.getName()).get();
        request.getSession().setAttribute("sessionGoods", Lists.newArrayList());
        ordersService.save(user.getId(), converter.toListGood(goodsDTO));
    }

    @ExceptionHandler(OrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity handleOrderNotFoundException() {
        return ResponseEntity.notFound().build();
    }
}
