package by.training.api;

import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;
import by.training.services.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "GoodsController", tags = {"Goods Controller"})
@RestController
@RequestMapping(value = "/api/goods")
public class GoodsRestController {
    private final GoodsService goodsService;

    public GoodsRestController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @ApiOperation(value = "Get all goods")
    @GetMapping
    public ResponseEntity<List<Good>> get() {
        return ResponseEntity.ok(goodsService.findAll());
    }

    @ApiOperation(value = "Get good by id")
    @GetMapping(path = "/{good_id}")
    public ResponseEntity<Good> get(@PathVariable Long good_id)
            throws GoodNotFoundException {
        return ResponseEntity.ok(goodsService.findById(good_id));
    }

    @ExceptionHandler(GoodNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity handleGoodNotFoundException() {
        return ResponseEntity.notFound().build();
    }
}
