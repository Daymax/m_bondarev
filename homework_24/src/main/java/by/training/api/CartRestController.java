package by.training.api;

import by.training.dto.GoodDto;
import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;
import by.training.services.GoodsService;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(value = "Cart Controller", tags = {"Cart Controller"})
@RestController
@RequestMapping("/api/cart")
public class CartRestController {
    private final GoodsService goodsService;

    public CartRestController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @ApiOperation(value = "Get goods in cart")
    @GetMapping
    public ResponseEntity<List<Good>> get(HttpServletRequest request) {
        List<Good> sessionGoods = (List) request.getSession().getAttribute("sessionGoods");
        if (sessionGoods == null) {
            return ResponseEntity.ok(Lists.newArrayList());
        }
        return ResponseEntity.ok(sessionGoods);
    }

    @ApiOperation(value = "Put good to cart")
    @PostMapping
    public ResponseEntity<Good> post(@RequestBody GoodDto goodDTO, HttpServletRequest request) throws GoodNotFoundException {
        Good good = goodsService.findById(goodDTO.getId());
        List<Good> sessionGoods = (List) request.getSession().getAttribute("sessionGoods");
        if (sessionGoods == null) {
            sessionGoods = Lists.newArrayList();
        }
        sessionGoods.add(good);
        request.getSession().setAttribute("sessionGoods", sessionGoods);
        return ResponseEntity.status(HttpStatus.CREATED).body(good);
    }

    @ApiOperation(value = "Delete good from cart")
    @DeleteMapping("/{deletedId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Integer deletedId, HttpServletRequest request) {
        List<Good> sessionGoods = (List) request.getSession().getAttribute("sessionGoods");
        if (sessionGoods != null) {
            sessionGoods.remove(deletedId - 1);
            request.getSession().setAttribute("sessionGoods", sessionGoods);
        }
    }

}
