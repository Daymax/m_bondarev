package by.training.services.impl;

import by.training.converters.Converter;
import by.training.dao.OrderDao;
import by.training.dto.OrderDto;
import by.training.entities.Good;
import by.training.entities.Order;
import by.training.exceptions.OrderNotFoundException;
import by.training.services.OrdersService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Order service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
@Transactional
public class OrdersServiceImpl implements OrdersService {

    private final OrderDao orderDao;
    private final Converter converter;

    public OrdersServiceImpl(OrderDao orderDao, Converter converter) {
        this.orderDao = orderDao;
        this.converter = converter;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void save(Long userId, List<Good> addedGoods) {
        orderDao.save(userId, addedGoods);
    }

    @Override
    public List<OrderDto> findAllByUserId(Long userId) {

        return orderDao.findAllByUserId(userId).stream()
                .sorted(Comparator.comparing(Order::getId))
                .map(converter::toOrderDTO)
                .collect(Collectors.toList());
    }

    @Override
    public Order findById(Long id) throws OrderNotFoundException {
        return orderDao.findById(id)
                .orElseThrow(OrderNotFoundException::new);
    }
}
