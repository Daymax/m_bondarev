package by.training.services.impl;

import by.training.dao.GoodDao;
import by.training.dao.impl.GoodDaoImpl;
import by.training.entities.Good;
import by.training.exceptions.GoodNotFoundException;
import by.training.services.GoodsService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Goods service class.
 *
 * @author m_bondarev
 * @version 1.0
 */

@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {
    private final GoodDao goodDao;

    public GoodsServiceImpl(GoodDaoImpl goodDao) {
        this.goodDao = goodDao;
    }

    @Override
    public Good findById(Long id) throws GoodNotFoundException {
        return goodDao.findById(id)
                .orElseThrow(GoodNotFoundException::new);
    }

    @Override
    public List<Good> findAll() {
        return goodDao.findAll();
    }
}
