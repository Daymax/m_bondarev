package by.training.exceptions;

public class GoodNotFoundException extends RuntimeException {
    public GoodNotFoundException() {
    }

    public GoodNotFoundException(String message) {
        super(message);
    }
}
