package by.training.dto;

public class GoodDto {
    private Long id;

    public GoodDto() {
    }

    public GoodDto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
