package by.training.dto;

public class OrderDto {
    private Long id;
    private int totalPrice;

    public OrderDto() {
    }

    public OrderDto(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OrderDto(Long id) {
        this.id = id;
    }

    public OrderDto(Long id, int totalPrice) {
        this.id = id;
        this.totalPrice = totalPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
