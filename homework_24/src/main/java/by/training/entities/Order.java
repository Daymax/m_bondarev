package by.training.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Order entity.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Entity
@Table(name = "orders")
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total_price")
    private int totalPrice;

    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "orders_goods",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "good_id"))
    private List<Good> goods;

    public Order() {
    }

    public Order(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Order(List<Good> goods) {
        this.goods = goods;
    }

    public Order(int totalPrice, List<Good> goods) {
        this.totalPrice = totalPrice;
        this.goods = goods;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                totalPrice == order.totalPrice &&
                Objects.equals(goods, order.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, totalPrice, goods);
    }
}
