# How to run

  Run command:
```sh
mvn jetty:run
```
Then go to:
```sh
http://localhost:8080/online-shop/swagger-ui.html
```
You could login with:


| login        | password| 
| ------------- |:-------:|
| user      | user    |
| admin      | admin   |