package by.training;

import by.training.strategies.ConnectionStrategy;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;

/**
 * Main application class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Log4j
public class Main {
    /**
     * Application start point.
     * write "-manual" or "-httpclient" to select a mode of operation
     * then enter the request method "get" or "post"
     * and then enter id of article.
     * @param commandArguments Command line arguments.
     */
    public static void main(String[] commandArguments) {
        try {
            if (commandArguments.length != 3) {
                throw new IllegalArgumentException("Invalid number of the commandline arguments!");
            }
            if (!StringUtils.isNumeric(commandArguments[2])) {
                throw new IllegalArgumentException("Invalid Article id");
            }
            ConnectionStrategy connectionStrategy =
                    ConnectionStrategyBuilder.getConnectStrategy(commandArguments[0].toLowerCase());
            String httpMethod = commandArguments[1].toUpperCase();
            int articleId = Integer.parseInt(commandArguments[2]);
            String url = "https://jsonplaceholder.typicode.com/posts/";
            ArticleService articleService = new ArticleService(connectionStrategy, url);

            switch (httpMethod) {
                case "GET": {
                    if (articleId > 100 || articleId <= 0) {
                        throw new IllegalArgumentException("Article id must be greater than 0 "
                                + "and less than 100");
                    }
                    System.out.println("\nArticle [" + articleId + "]: "
                            + articleService.getArticle(articleId));
                    break;
                }
                case "POST": {
                    if (articleId <= 0) {
                        throw new IllegalArgumentException("Article id must be greater than 0");
                    }
                    System.out.println("\nArticle [" + articleId + "] has been created: "
                            + articleService.postArticle(articleId));
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Invalid HTTP Method");
                }
            }
        } catch (RuntimeException exception) {
            log.error(exception.getMessage());
        }
    }
}
