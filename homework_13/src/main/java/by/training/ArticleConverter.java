package by.training;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

/**
 * Article to/from json converter utility.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class ArticleConverter {

    /**
     * Returns the Article from json.
     *
     * @param jsonStr json string
     * @return Article
     */
    public static Article fromJsonToArticle(String jsonStr) {
        Gson gson = new Gson();
        if (jsonStr.startsWith("[")) {
            Article[] article = gson.fromJson(jsonStr, Article[].class);
            if (article.length != 1) {
                throw new JsonIOException("Article parsing exception");
            }
            return article[0];
        } else {
            return gson.fromJson(jsonStr, Article.class);
        }
    }

    /**
     * Returns json of the Article.
     *
     * @param article Article
     * @return json
     */
    public static String fromArticleToJson(Article article) {
        Gson gson = new Gson();
        return gson.toJson(article);
    }
}
