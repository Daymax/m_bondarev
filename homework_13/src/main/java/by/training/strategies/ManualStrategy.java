package by.training.strategies;

import lombok.extern.log4j.Log4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * HttpClient connect strategy class.
 *
 * @author m_bondarev
 * @version 1.0
 * @see ConnectionStrategy
 */
@Log4j
public class ManualStrategy implements ConnectionStrategy {
    @Override
    public String doGet(String url, Map<String, Object> parameters) throws IOException {
        String urlWithParameters = getUrlWithParameters(url, parameters);
        log.info("Sending GET request to " + urlWithParameters);
        HttpURLConnection connection =
                (HttpURLConnection) new URL(urlWithParameters).openConnection();
        connection.setRequestProperty("accept", "application/json");
        if (connection.getResponseCode() != 200) {
            log.error("Cannot connect to " + urlWithParameters);
            log.error("Response code: " + connection.getResponseCode());
            throw new IOException();
        }
        log.info("Response code: " + connection.getResponseCode());
        String response =
                IOUtils.toString(new InputStreamReader(connection.getInputStream()));

        closeConnection(connection);

        return response;
    }

    @Override
    public String doPost(String url, String jsonString) throws IOException {
        log.info("Sending POST request to " + url);
        HttpURLConnection connection =
                (HttpURLConnection) new URL(url).openConnection();

        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");

        IOUtils.write(jsonString, connection.getOutputStream());
        if (connection.getResponseCode() != 201) {
            log.error("Cannot connect to " + url);
            log.error("Response code: " + connection.getResponseCode());
            throw new IOException();
        }
        log.info("Response code: " + connection.getResponseCode());
        String response = IOUtils.toString(connection.getInputStream());

        closeConnection(connection);

        return response;
    }

    /**
     * Closes the connection.
     *
     * @param connection connection
     */
    private static void closeConnection(HttpURLConnection connection) {
        if (connection != null) {
            try {
                connection.disconnect();
            } catch (Exception exception) {
                log.error("Connection closing exception");
            }
        }
    }
}
