package by.training;

import by.training.strategies.ConnectionStrategy;
import lombok.extern.log4j.Log4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Article service.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Log4j
public class ArticleService {
    private ConnectionStrategy connectionStrategy;
    private String url;

    public ArticleService(ConnectionStrategy connectionStrategy, String url) {
        this.connectionStrategy = connectionStrategy;
        this.url = url;
    }

    /**
     * Gets Article from the service.
     *
     * @param id id of the Article
     * @return Article
     */
    public Article getArticle(int id) {
        Map<String, Object> urlParameters = new HashMap<>();
        urlParameters.put("id", id);
        String response;
        try {
            response = connectionStrategy.doGet(url, urlParameters);
        } catch (IOException exception) {
            throw new RuntimeException("GET request exception");
        }
        return ArticleConverter.fromJsonToArticle(response);
    }

    /**
     * Posts Article to the service.
     *
     * @param id id of the Article
     * @return Article
     */
    public Article postArticle(int id) {
        Article article = new Article(id, 1, "Im your father!", "Noooooooo");
        String response;
        try {
            response = connectionStrategy.doPost(url, ArticleConverter.fromArticleToJson(article));
        } catch (IOException exception) {
            throw new RuntimeException("POST request exception");
        }
        return ArticleConverter.fromJsonToArticle(response);
    }
}
