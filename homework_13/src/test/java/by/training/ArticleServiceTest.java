package by.training;

import by.training.strategies.ConnectionStrategy;
import by.training.strategies.HttpClientStrategy;
import by.training.strategies.ManualStrategy;
import com.google.gson.JsonIOException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ArticleServiceTest {
    private ConnectionStrategy connectionStrategy;
    private ArticleService articleService;

    @Before
    public void setUp() {
        connectionStrategy = mock(ConnectionStrategy.class);
        String url = "https://jsonplaceholder.typicode.com/posts/";
        articleService = new ArticleService(connectionStrategy, url);
    }

    @Test
    public void testArticleServiceGet() throws IOException {
        when(connectionStrategy.doGet(anyString(), anyMap())).thenReturn("[{\n" +
                "    \"id\": 101,\n" +
                "    \"userId\": 1,\n" +
                "    \"title\": \"Im your father!\",\n" +
                "    \"body\": \"Noooooooo\"\n" +
                "}]");
        Article expected = new Article(101, 1, "Im your father!", "Noooooooo");
        Article actual = articleService.getArticle(1);
        Assert.assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void testArticleServicePost() throws IOException {
        when(connectionStrategy.doPost(anyString(), anyString())).thenReturn("{\n" +
                "    \"id\": 101,\n" +
                "    \"userId\": 1,\n" +
                "    \"title\": \"Im your father!\",\n" +
                "    \"body\": \"Noooooooo\"\n" +
                "}");
        Article expected = new Article(101, 1, "Im your father!", "Noooooooo");
        Article actual = articleService.postArticle(101);
        Assert.assertEquals(expected.toString(), actual.toString());
    }

    @Test(expected = RuntimeException.class)
    public void testArticleServiceGetConnectionFails_RuntimeException() throws IOException {
        when(connectionStrategy.doGet(anyString(), anyMap())).thenThrow(IOException.class);
        articleService.getArticle(1);
    }

    @Test(expected = RuntimeException.class)
    public void testArticleServicePostConnectionFails_RuntimeException() throws IOException {
        when(connectionStrategy.doPost(anyString(), anyString())).thenThrow(IOException.class);
        articleService.postArticle(101);
    }

    @Test(expected = JsonIOException.class)
    public void serviceTestFailsEmptyResponse_JsonIOException() throws IOException {
        when(connectionStrategy.doPost(anyString(), anyString())).thenReturn("[]");
        articleService.postArticle(1);
    }

    @Test
    public void testGetConnectStrategyManual() {
        Assert.assertTrue(ConnectionStrategyBuilder
                .getConnectStrategy("-manual") instanceof ManualStrategy);
    }

    @Test
    public void testGetConnectStrategyHttpClient() {
        Assert.assertTrue(ConnectionStrategyBuilder
                .getConnectStrategy("-httpclient") instanceof HttpClientStrategy);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetConnectStrategyInvalidStrategyName() {
        ConnectionStrategyBuilder.getConnectStrategy("-wrong");
    }
}
