package com.training.task1;

import java.math.BigDecimal;

/**
 * Card utility class.
 */

public class Card {

    private String ownerName;
    private BigDecimal balance;

    /**
     * Get owner's name.
     * @return Owner Name.
     */

    public String getOwnerName() {
        return ownerName;
    }

    /**
     * Get current balance.
     * @return Current balance.
     */

    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Get current balance in other currency.
     * @return - Balance in other currency.
     */

    public BigDecimal getBalanceInOtherCurrency(BigDecimal currencyRatio) {
        if(currencyRatio.compareTo(BigDecimal.ZERO) >= 0) {
            return balance.multiply(currencyRatio);
        }
        throw new ArithmeticException("Incorrect data");
    }

    /**
     * Constructor with owner name and balance.
     * @param ownerName - Card owner's name.
     * @param balance - Card balance.
     */

    public Card(String ownerName, BigDecimal balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    /**
     * Constructor with owner name.
     * @param ownerName - Card owner's name.
     */

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * Method for withdrawing balance.
     * @param money - Money that's withdrawn.
     * @return - Balance with withdrawn funds.
     */

    public BigDecimal withdrawBalance(BigDecimal money) {

        if(money.compareTo(BigDecimal.ZERO) >= 0) {
            this.balance = balance.subtract(money);
            return balance;
        }
        throw new ArithmeticException();
    }

    /**
     * Method for adding balance.
     * @param money - Money that's added.
     * @return - Balance with added funds.
     */

    public BigDecimal addBalance(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) >= 0) {
            this.balance = balance.add(money);
            return balance;
        }
        throw new ArithmeticException();
    }

}
