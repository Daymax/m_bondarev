package com.training.task2;

import java.util.Arrays;

/**
 * Median utility class.
 */

public class Median {

    /**
     * Calculates the median of the int array.
     * @param array - Transferable int array.
     * @return - Median of the int array.
     */

    public static float median(int[] array) {
        Arrays.sort(array);

        if (array.length % 2 == 1) {
            return array[array.length / 2];
        } else {
            return (float)(array[(array.length / 2) - 1] + array[array.length / 2]) / 2;
        }
    }

    /**
     * Calculates the median of the double array.
     * @param array - Transferable double array.
     * @return - Median of the double array.
     */
    public static double median(double[] array) {
        Arrays.sort(array);

        if (array.length % 2 == 1) {
            return array[array.length / 2];
        } else {
            return (array[(array.length / 2) - 1] + array[array.length / 2]) / 2;
        }
    }
}
