package com.training.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test class for testing card methods.
 */

public class CardTest {

    private Card cardWithBalance;
    private Card cardWithoutBalance;

    /**
     * Test method for initializing cards.
     */

    @Before
    public void before(){
        cardWithBalance = new Card("Valera", BigDecimal.valueOf(200));
        cardWithoutBalance = new Card("Vasya");
    }

    /**
     * Test method to test the method getOwner() for card with balance;
     */

    @Test
    public void testGetOwnerName() {
        String actual = cardWithBalance.getOwnerName();
        String expected = "Valera";
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method to test the method getOwner() for card without balance;
     */

    @Test
    public void testGetOwnerNameWithoutBalance() {
        String actual = cardWithoutBalance.getOwnerName();
        String expected = "Vasya";
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method to test the method getBalance();
     */

    @Test
    public void testGetBalance() {
        BigDecimal actual = cardWithBalance.getBalance();
        BigDecimal expected = BigDecimal.valueOf(200);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method to test the method getBalanceInOtherCurrency();
     */

    @Test
    public void testGetBalanceInOtherCurrency() {
        BigDecimal actual = cardWithBalance.getBalanceInOtherCurrency(BigDecimal.valueOf(2));
        BigDecimal expected = BigDecimal.valueOf(400);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method to test the method getBalanceInOtherCurrency();
     */

    @Test(expected = ArithmeticException.class)
    public void testGetBalanceInOtherCurrencyWithArithmeticException() {
        BigDecimal actual = cardWithBalance.getBalanceInOtherCurrency(BigDecimal.valueOf(-2));
        BigDecimal expected = BigDecimal.valueOf(400);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method to test the method withdrawBalance();
     */

    @Test
    public void testWithdrawBalance() {
        BigDecimal actual = cardWithBalance.withdrawBalance(BigDecimal.valueOf(50));
        BigDecimal expected = BigDecimal.valueOf(150);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method to test the method withdrawBalance() when we try to withdraw a negative amount;
     */

    @Test(expected = ArithmeticException.class)
    public void testWithdrawBalanceWithArithmeticException() {
        BigDecimal actual = cardWithBalance.withdrawBalance(BigDecimal.valueOf(-50));
        BigDecimal expected = BigDecimal.valueOf(50);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test method to test the method addBalance();
     */

    @Test
    public void testAddBalance() {
        BigDecimal actual = cardWithBalance.addBalance(BigDecimal.valueOf(50));
        BigDecimal expected = BigDecimal.valueOf(250);
        Assert.assertEquals(expected, actual);
    }


    /**
     * Test method to test the method addBalance() when we try to add a negative amount;
     */

    @Test(expected = ArithmeticException.class)
    public void testAddBalanceWithArithmeticException() {
        BigDecimal actual = cardWithBalance.addBalance(BigDecimal.valueOf(-50));
        BigDecimal expected = BigDecimal.valueOf(50);
        Assert.assertEquals(expected, actual);
    }
}