package by.training.task1;

import java.math.BigDecimal;

/**
 * Class Credit card that inherits abstract class Card.
 * @version 1.0.
 * @author Max Bondarev.
 */
public class CreditCard extends Card {

    /**
     * Constructor for CreditCard.
     * @param ownerName - Card owner's name.
     * @param balance - Card balance.
     */
    public CreditCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

    /**
     * Method for adding balance.
     * @param money - Money that's added.
     */
    @Override
    public synchronized void addBalance(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) >= 0) {
            this.balance = balance.add(money);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Method for withdrawing balance.
     * @param money - Money that's withdrawn.
     * @throws IllegalArgumentException when money < 0.
     */
    @Override
    public synchronized void withdrawBalance(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) >= 0) {
            balance = balance.subtract(money);
        } else {
            throw new IllegalArgumentException();
        }
    }
}
