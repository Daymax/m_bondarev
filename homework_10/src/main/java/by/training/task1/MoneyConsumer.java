package by.training.task1;

import java.math.BigDecimal;

/**
 * MoneyConsumer utility class.
 * @author m_bondarev
 * @version 1.0
 */
public class MoneyConsumer extends Atm implements Runnable {

    /**
     * Constructor
     * @param card which we used.
     */
    public MoneyConsumer(Card card) {
        super(card);
        new Thread(this);
    }

    /**
     * Method run for Money consumer when we withdraw money from balance.
     */
    @Override
    public void run() {
        // 2 <= sleeptime < 5 sec
        int sleepTime = 2000 + random.nextInt(3001);
        while (card.getBalance().compareTo(MIN_BALANCE) > 0 && card.getBalance().compareTo(MAX_BALANCE) < 0) {
            try {
                int range = 5 + random.nextInt(6);
                withdrawFromCard(BigDecimal.valueOf(range));
                System.out.println(range + " withdrawn from account Balance: " + card.getBalance() + " Thread " + Thread.currentThread().getName());
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

