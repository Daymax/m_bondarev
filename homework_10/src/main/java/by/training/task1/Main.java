package by.training.task1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Main utility class.
 * @author m_bondarev
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        // card with 500
        Card card = new CreditCard("Valera", BigDecimal.valueOf(500));
        //7 threads for 7 atms. Each process in a separate thread
        ExecutorService service = Executors.newFixedThreadPool(7);

        List<Runnable> atms = new ArrayList<>();

        atms.add(new MoneyProducer(card));
        atms.add(new MoneyProducer(card));
        atms.add(new MoneyProducer(card));
        atms.add(new MoneyConsumer(card));
        atms.add(new MoneyConsumer(card));
        atms.add(new MoneyConsumer(card));
        atms.add(new MoneyConsumer(card));

        for (Runnable atm : atms) {
            service.submit(atm);
        }
        service.shutdown();
    }
}
