package by.training.task1;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Atm utility class.
 * @version 1.0.
 * @author Max Bondarev.
 */
public class Atm {
    protected Card card;
    public static final BigDecimal MAX_BALANCE = BigDecimal.valueOf(1000);
    public static final BigDecimal MIN_BALANCE = BigDecimal.ZERO;
    protected Random random = new Random();

    /**
     * Constructor that accept the card.
     * @param card - Specific card to be inserted into the ATMю
     */
    public Atm(Card card) {
        this.card = card;
    }

    /**
     * Method for withdrawing balance.
     * @param money - Money that's withdrawn from card.
     */
    public void withdrawFromCard(BigDecimal money) {
        card.withdrawBalance(money);
    }

    /**
     * Method for adding balance to card.
     * @param money - Money that's added to card.
     */
    public void addToCard(BigDecimal money) {
        card.addBalance(money);
    }
}
