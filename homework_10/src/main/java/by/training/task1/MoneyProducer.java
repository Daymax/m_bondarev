package by.training.task1;

import java.math.BigDecimal;

/**
 * MoneyProducer utility class.
 * @author m_bondarev.
 * @version 1.0
 */
public class MoneyProducer extends Atm implements Runnable {

    public MoneyProducer(Card card) {
        super(card);
        new Thread(this);
    }

    /**
     * Method run for money consumer when we add money to balance.
     */
    @Override
    public void run() {
        // 2 <= sleeptime < 5 sec
        int sleepTime = 2000 + random.nextInt(3001);
        while (card.getBalance().compareTo(MIN_BALANCE) > 0 && card.getBalance().compareTo(MAX_BALANCE) < 0) {
            try {
                int range = 5 + random.nextInt(6);
                addToCard(BigDecimal.valueOf(range));
                System.out.println("Balance replenished at " + range + " Balance: " + card.getBalance() + " Thread " + Thread.currentThread().getName());
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

