package by.training.task1;

import java.math.BigDecimal;

/**
 * Card utility class.
 * @version 1.0
 * @author Max Bondarev.
 */
public abstract class Card {

    protected String ownerName;
    protected BigDecimal balance;

    /**
     * Get owner's name.
     * @return Owner Name.
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * Get current balance.
     * @return Current balance.
     */
    public synchronized BigDecimal getBalance() {
        return balance;
    }

    /**
     * Constructor with owner name and balance.
     * @param ownerName - Card owner's name.
     * @param balance - Card balance.
     */
    public Card(String ownerName, BigDecimal balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    /**
     * Constructor with owner name.
     * @param ownerName - Card owner's name.
     */
    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * Get current balance in other currency.
     * @return - Balance in other currency.
     * @throws IllegalArgumentException when currencyRatio < 0.
     */
    public BigDecimal getBalanceInOtherCurrency(BigDecimal currencyRatio) {
        if (currencyRatio.compareTo(BigDecimal.ZERO) >= 0) {
            return balance.multiply(currencyRatio);
        }
        throw new IllegalArgumentException("Incorrect data");
    }

    /**
     * Method for withdrawing balance.
     * @param money - Money that's withdrawn.
     */
    public abstract void withdrawBalance(BigDecimal money);

    /**
     * Method for adding balance.
     * @param money - Money that's added.
     */
    public abstract void addBalance(BigDecimal money);

}
