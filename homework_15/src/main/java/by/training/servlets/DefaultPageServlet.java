package by.training.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * DefaultPageServlet class.
 * UrlPatterns = {/*}.
 */
public class DefaultPageServlet extends HttpServlet {

    /**
     * Method which shows default page.
     *
     * @param request  request {@link HttpServletRequest}.
     * @param response response {@link HttpServletResponse}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     * @throws IOException      when arises problems with input-output stream.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter writer = response.getWriter()) {

            writer.write("<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "    <head>\n"
                    + "        <title>Online Shop</title>\n"
                    + "    </head>\n"
                    + "    <body>\n"
                    + "        <h1 style=\"text-align: center\">Welcome to Online Shop!</h1><br/>\n"
                    + "        <form style=\"text-align: center\" action=\"/hello\" method=\"post\">\n"
                    + "            <input name=\"name\"><br/><br/>\n"
                    + "            <button type=\"submit\">Enter</button>"
                    + "        </form>\n"
                    + "    </body>\n"
                    + "</html>");
        }
    }
}
