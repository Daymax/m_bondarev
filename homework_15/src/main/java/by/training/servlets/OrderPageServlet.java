package by.training.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * OrderPageServlet class.
 * urlPatterns = {/order}
 */
public class OrderPageServlet extends HttpServlet {
    /**
     * Method takes parameters of order and shows ready to order.
     *
     * @param request  request {@link HttpServletRequest}.
     * @param response response {@link HttpServletResponse}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     * @throws IOException      when arises problems with input-output stream.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Optional<String> userName = Optional.of(request.getParameter("name"));
        Optional<String[]> goods = Optional.ofNullable(request.getParameterValues("order"));
        PrintWriter writer = response.getWriter();

        writer.write("<!DOCTYPE html>\n"
                + "<html>\n"
                + "    <head>\n"
                + "        <title>Online Shop</title>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <h2 style=\"text-align: center\">Dear " + userName.get()
                + "                                         , your order: </h2>\n"
                + "        <div style=\"width: 200px; margin: auto\">\n");

        if (goods.isPresent()) {
            writer.write("  <ol style=\"text-align: left\" type=\"1\">\n");
            final String[] products = goods.get();
            final String itemList = Arrays.stream(products)
                    .map(good -> good.replaceAll("[\\[\\](){}]", ""))
                                     .map(good -> "  <li>".concat(good)
                                                          .concat("  </li>\n"))
                                                          .collect(Collectors.joining());
            writer.write(itemList);
            writer.write("   </ol>\n"
                    + "<h4>Total: ");
            final double total = Arrays.stream(products)
                    .map(good -> good.replaceAll("[^\\d.]", ""))
                                     .mapToDouble(Double::valueOf).sum();
            writer.write(total + "$</h4>\n");

        } else {
            writer.write("<h3>You have no orders!</h3>\n");
        }

        writer.write("  </div>"
                + "    </body>\n"
                + "</html>");
    }
}
