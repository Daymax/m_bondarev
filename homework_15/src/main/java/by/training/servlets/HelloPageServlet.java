package by.training.servlets;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * HelloPageServlet class.
 * urlPatterns = {/hello}
 */
public class HelloPageServlet extends HttpServlet {
    private Map<String, Double> itemNameToPrice;

    /**
     * Method for initialization servlet.
     * @param config - servlet config {@link ServletConfig}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        final ServletContext servletContext = config.getServletContext();
        itemNameToPrice = Collections.list(servletContext.getInitParameterNames())
                .stream()
                .collect(Collectors.toMap(name ->
                        name, name -> Double.valueOf(servletContext.getInitParameter(name))));
        super.init(config);
    }

    /**
     * Method takes parameters of username, shows greeting and goods.
     *
     * @param request  request {@link HttpServletRequest}.
     * @param response response {@link HttpServletResponse}.
     * @throws ServletException when servlet can throw when it encounters difficulty.
     * @throws IOException      when arises problems with input-output stream.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Optional<String> userName = Optional.of(request.getParameter("name"));
        String name = userName.get().equals(StringUtils.EMPTY) ? "Anonymous" : userName.get();

        try (PrintWriter writer = response.getWriter()) {

            writer.write("<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "    <head>\n"
                    + "        <title>Online Shop</title>\n"
                    + "    </head>\n"
                    + "    <body>\n"
                    + "        <h1 style=\"text-align: center\">Hello " + name + "!</h1>\n"
                    + "        <h5 style=\"text-align: center\">Make your order: </h5><br/>\n"
                    + "        <form style=\"text-align: center\" action=\"/order\" method=\"post\">\n"
                    + "            <input hidden name=\"name\" value=\"" + name + "\">\n"
                    + "            <select multiple name=\"order\">");

            for (Map.Entry element : itemNameToPrice.entrySet()) {
                writer.write("<option>"
                        + element.getKey()
                        + " "
                        + "("
                        + element.getValue()
                        + "$)"
                        + "</option><br/>\n");
            }
            writer.write("      </select><br/>\n"
                    + "<button style=\"margin-top: 20px\" type=\"submit\">Submit</button><br/>\n"
                    + "        </form>\n"
                    + "     </body>\n"
                    + "</html>");
        }
    }
}
