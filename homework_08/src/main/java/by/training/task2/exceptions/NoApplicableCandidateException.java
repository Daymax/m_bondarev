package by.training.task2.exceptions;

/**
 * Class NoApplicableCandidateException for situations when tender lacks the necessary candidates.
 */

public class NoApplicableCandidateException extends Exception {
    public NoApplicableCandidateException(String message) {
        super(message);
    }
}
