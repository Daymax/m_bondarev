package by.training.task2.exceptions;

/**
 * Class ModifyClosedTenderException for situations when we are trying to change a closed tender.
 */

public class ModifyClosedTenderException extends RuntimeException {
    public ModifyClosedTenderException(String message) {
        super(message);
    }
}
