package by.training.task2.exceptions;

/**
 * Class EmptyCrewException for situations when we are trying to get an estimate from an empty crew.
 */

public class EmptyCrewException extends RuntimeException {
    public EmptyCrewException(String message) {
        super(message);
    }
}
