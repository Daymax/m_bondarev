package by.training.task2;

import by.training.task2.exceptions.ModifyClosedTenderException;
import by.training.task2.exceptions.NoApplicableCandidateException;

import java.util.*;


/**
 * Tender utility class.
 * @author m_bondarev
 * @version 1.0
 */

public class Tender {

    private String name;
    private Map<Worker.ProfessionalSkills, Integer> necessaryConditions = new HashMap<>();
    private List<Crew> candidates = new ArrayList<>();
    private Map<String, Integer> result = new HashMap<>();
    private boolean isActive = false;

    /**
     * Getter candidate list.
     * @return candidate list.
     */
    public List<Crew> getCandidates() {
        return candidates;
    }

    /**
     * Getter for result.
     * @return map with suitable candidates.
     */
    public Map<String, Integer> getResult() {
        return result;
    }

    /**
     * Getter for necessary conditions.
     * @return necessary conditions.
     */
    public Map<Worker.ProfessionalSkills, Integer> getNecessaryConditions() {
        return necessaryConditions;
    }

    /**
     * Constructor with name.
     * @param name - name of project.
     */
    public Tender(String name) {
        isActive = true;
        this.name = name;
    }

    /**
     * Method for adding candidate to project.
     * @param crew - Crew of workers.
     */
    public boolean addCandidate(Crew crew) {
        candidates.add(crew);
        return true;
    }

    /**
     * Method for adding conditions for project.
     * @param skill - required skill for worker.
     * @param number - required number of skills.
     */
    public void addConditions(Worker.ProfessionalSkills skill, int number) {
        necessaryConditions.put(skill, number);
    }

    /**
     * Method for showing result.
     * @return min in the estimate of candidates.
     */
    public String searchMinInMap(Map<String, Integer> result) {
        return Collections.min(result.entrySet(),
                Comparator.comparingInt(Map.Entry::getValue)).getKey();
    }

    /**
     * Method for finding applicable candidates.
     * @throws NoApplicableCandidateException if no matching candidate is found.
     */
    public void findApplicable() throws NoApplicableCandidateException {

        if (!isActive) {
            throw new ModifyClosedTenderException("tender is close");
        } else {
            for (Crew candidate : candidates) {
                if (candidate.isCrewApplicable(necessaryConditions)) {
                    result.put(candidate.getName(), candidate.getEstimate());
                }
            }
            if (result.isEmpty()) {
                throw new NoApplicableCandidateException("No applicable candidates");
            }
        }
    }

    /**
     * Method for showing is active project or not.
     * @return true if project is active and false if not.
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Method for close project.
     */
    public void close() {
        isActive = false;
    }
}
