package by.training.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Worker utility class.
 * @author m_bondarev
 * @version 1.0
 */
public class Worker {

    private int financialPosition;
    private List<ProfessionalSkills> skills;

    /**
     * Constructor for create worker with financial position and some professional skills.
     * @param financialPosition - salary of worker.
     * @param professionalSkills - skills of worker.
     */
    public Worker(int financialPosition, ProfessionalSkills... professionalSkills) {

        skills = new ArrayList<>();
        skills = Arrays.asList(professionalSkills);

        if (financialPosition > 0) {
            this.financialPosition = financialPosition;
        } else {
            throw new IllegalArgumentException("Financial position must be more than 0");
        }
    }

    /**
     * Getter financial position of worker.
     * @return - salary of worker.
     */
    public int getFinancialPosition() {
        return financialPosition;
    }

    /**
     * Enum with professional skills.
     */
    public enum ProfessionalSkills {
        CARPENTER,
        WELDER,
        BUILDER,
        STONEMASON,
        ELECTRIC
    }

    /**
     * A method that indicates whether a worker is suitable.
     * @param skill - required skill.
     * @return true if skill required, false if not.
     */
    public boolean isApplicable(ProfessionalSkills skill) {

        if (skills.contains(skill)) {
            return true;
        } else {
            return false;
        }
    }
}
