package by.training.task2;

import by.training.task2.exceptions.EmptyCrewException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Crew utility class.
 * @author m_bondarev
 * @version 1.0
 */
public class Crew {

    private Integer estimate = 0;
    private String name;
    private List<Worker> workers;

    /**
     * Getter for name of crew.
     * @return name of crew.
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for estimate of crew.
     * @return total wage requirement for all workers in the crew.
     */
    public Integer getEstimate() {

        if (workers.isEmpty()) {
            throw new EmptyCrewException("Crew is empty");
        } else {
            for (Worker worker : workers) {
                estimate += worker.getFinancialPosition();
            }
            return estimate;
        }
    }

    /**
     * Constructor with name.
     * @param name - name of crew.
     */
    public Crew(String name) {
        this.workers = new ArrayList<>();
        this.name = name;
    }

    /**
     * Method for adding worker to crew.
     * @param worker - worker.
     */
    public void addWorker(Worker worker) {
        workers.add(worker);
    }

    /**
     * Method which shows whether the crew is suitable for the project.
     * @param conditions require for project.
     * @return true if crew is applicable.
     */
    public boolean isCrewApplicable(Map<Worker.ProfessionalSkills, Integer> conditions) {


        for (Map.Entry<Worker.ProfessionalSkills, Integer> entry : conditions.entrySet()) {
            Worker.ProfessionalSkills skill = entry.getKey();
            int numberOfSKills = entry.getValue();
            int number = 0;

            for (Worker worker : workers) {
                if (worker.isApplicable(skill)) {
                    number += 1;
                }
            }

            if (number < numberOfSKills) {
                return false;
            }

        }
        return true;
    }
}
