package by.training.task1.exceptions;

/**
 * Class for exceptions when list is full.
 * @author m_bondarev
 * @version 1.0
 */

public class ListFullException extends RuntimeException {
    public ListFullException(String message) {
        super(message);
    }
}
