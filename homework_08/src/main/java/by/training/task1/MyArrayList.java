package by.training.task1;

import by.training.task1.exceptions.ListFullException;

import java.util.ArrayList;

/**
 * Myarraylist utility class.
 * @param <T> type of object that we store.
 * @author m_bondarev
 * @version 1.0
 */
public class MyArrayList<T> extends ArrayList<T> {

    private T[] values;
    private int size;
    private int position = 0;

    /**
     * Constructor for initialize list.
     * @param initialSize size of list.
     * @throws IllegalArgumentException if initialsize < 0.
     */
    public MyArrayList(int initialSize) {

        if (initialSize >= 0) {
            this.values = (T[]) new Object[] {};
        } else {
            throw new IllegalArgumentException("Size isn't valid!");
        }
        this.size = initialSize;
    }

    /**
     * Method for add element to list.
     * @param element which to be added.
     * @return true if we added the element, false if not.
     * @throws ListFullException if list is full.
     */
    @Override
    public boolean add(T element) {
        if (position != size) {
            Object[] temp = values;
            values = (T[]) new Object[position + 1];
            System.arraycopy(temp, 0, values, 0, temp.length);
            values[position] = element;
            position++;
            return true;
        } else {
            throw new ListFullException("List is Full");
        }
    }

    /**
     * Method for getting list size.
     * @return - list size.
     */
    @Override
    public int size() {
        return values.length;
    }

    /**
     * Method for find out if list is empty.
     * @return true if list is empty, false if not.
     */

    @Override
    public boolean isEmpty() {
        return values.length == 0;
    }

    /**
     * Method to find out if an item is in the list.
     * @param element which we search.
     * @return true if element is in list, false if not.
     */
    @Override
    public boolean contains(Object element) {
        for (T object : values) {
            if (object.equals(element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method for removing element.
     * @param object - element which must be removing.
     * @return true if we removed the object, false if not.
     */
    @Override
    public boolean remove(Object object) {
        for (T element : values) {
            if (element.equals(object)) {
                int indexNeeded = indexOf(object);
                T[] temp = values;
                values = (T[]) new Object[temp.length - 1];
                System.arraycopy(temp, 0, values, 0, indexNeeded);
                System.arraycopy(temp, indexNeeded + 1, values,
                        indexNeeded,temp.length - indexNeeded - 1);
                size = values.length;
                return true;
            }
        }
        return false;
    }

    /**
     * Method for clear the list.
     */
    @Override
    public void clear() {
        for (int i = 0; i < values.length; i++) {
            remove(values[i]);
        }
        size = 0;
    }

    /**
     * Method for getting value by index.
     * @param index - index which we needed.
     * @return the value that is in the array by index.
     */
    @Override
    public T get(int index) throws IndexOutOfBoundsException {
        return values[index];
    }

    /**
     * Method for writing a value by index.
     * @param index in the array where we put the value.
     * @param element - value to be written.
     * @return added value.
     */
    @Override
    public T set(int index, T element) {
        values[index] = element;
        return values[index];
    }

    /**
     * Method for finding the index of an object in an array.
     * @param object whose index you want to find.
     * @return index of the object in the array.
     */
    @Override
    public int indexOf(Object object) {
        for (int i = 0; i < size; i++) {
            if (object.equals(values[i])) {
                return i;
            }
        }
        return -1;
    }
}
