package by.training.task1;

import by.training.task1.exceptions.ListFullException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Class for testing Myarraylist.
 * @author m_bondarev
 * @version 1.0
 */
public class MyArrayListTest {

    private List<String> list;

    /**
     * Method for initialise list.
     */
    @Before
    public void initialize() {
        list = new MyArrayList<>(5);
        list.add("first");
    }

    /**
     * Test method for adding an item to a list
     */
    @Test
    public void testAdd() {
        Assert.assertTrue(list.add("second"));
    }

    /**
     * Test method for adding element when list is full.
     * @throws ListFullException when list is full.
     */
    @Test(expected = ListFullException.class)
    public void testAddWithListFullException() {
        list.add("first");
        list.add("second");
        list.add("third");
        list.add("fourth");
        list.add("fifth");
        list.add("sixth");
    }

    /**
     * Test method for getting size list.
     */
    @Test
    public void testSize() {
        int actual = list.size();
        int expected = 1;
        Assert.assertEquals(expected, actual);
    }

    /**
     *Test method to find out if the list is empty.
     */
    @Test
    public void testIsEmpty() {
        boolean emptyOrNot = list.isEmpty();
        boolean expected = false;
        Assert.assertEquals(expected, emptyOrNot);
    }

    /**
     * Test method to find element.
     */
    @Test
    public void testContains() {
        boolean actual = list.contains("third");
        boolean expected = false;
        Assert.assertEquals(expected, actual);
    }

    /**
     * Method for testing remove object.
     */
    @Test
    public void testRemove() {
        boolean actual = list.remove("first");
        boolean expected = true;
        Assert.assertEquals(expected, actual);
    }

    /**
     * Method for testing cleat the list.
     */
    @Test
    public void testClear() {
        list.clear();
        boolean actual = list.isEmpty();
        boolean expected = true;
        Assert.assertEquals(expected, actual);
    }

    /**
     * Method for testing get object by index.
     */
    @Test
    public void testGet() {
        String actual = list.get(0);
        String expected = "first";
        Assert.assertEquals(expected, actual);
    }

    /**
     * Method for testing get object by index.
     * @throws IndexOutOfBoundsException when index > list.size().
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetWithIndexOutOfBoundsException() {
        list.get(6);
    }

    /**
     * Method for testing setting object by index.
     */
    @Test
    public void testSet() {
        String actual = list.set(0, "second");
        String expected = list.get(0);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Method for testing getting index by value.
     */
    @Test
    public void testIndexOf() {
        int actual = list.indexOf("first");
        int expected = 0;
        Assert.assertEquals(expected, actual);
    }
}