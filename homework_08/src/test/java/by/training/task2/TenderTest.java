package by.training.task2;

import by.training.task2.exceptions.ModifyClosedTenderException;
import by.training.task2.exceptions.NoApplicableCandidateException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TenderTest {
    private Tender tender;
    private static Crew mogilevTech = new Crew("Mogilev Tech");
    private static Crew iTechArt = new Crew("iTechArt");
    private static Crew olsa = new Crew("Olsa");

    @BeforeClass
    public static void setUpCandidates() {
        mogilevTech.addWorker(new Worker(500, Worker.ProfessionalSkills.WELDER, Worker.ProfessionalSkills.BUILDER));
        mogilevTech.addWorker(new Worker(200, Worker.ProfessionalSkills.CARPENTER, Worker.ProfessionalSkills.STONEMASON));
        mogilevTech.addWorker(new Worker(700, Worker.ProfessionalSkills.values()));
        mogilevTech.addWorker(new Worker(100, Worker.ProfessionalSkills.STONEMASON));
        mogilevTech.addWorker(new Worker(350, Worker.ProfessionalSkills.WELDER, Worker.ProfessionalSkills.ELECTRIC));
        //3 Welder, 2 ELECTRICIAN, 2 Carpenter, 3 Stonemason, 2 Builder

        iTechArt.addWorker(new Worker(200, Worker.ProfessionalSkills.BUILDER));
        iTechArt.addWorker(new Worker(800, Worker.ProfessionalSkills.values()));
        iTechArt.addWorker(new Worker(750, Worker.ProfessionalSkills.ELECTRIC, Worker.ProfessionalSkills.BUILDER, Worker.ProfessionalSkills.STONEMASON));
        iTechArt.addWorker(new Worker(400, Worker.ProfessionalSkills.STONEMASON));
        iTechArt.addWorker(new Worker(600, Worker.ProfessionalSkills.WELDER, Worker.ProfessionalSkills.ELECTRIC, Worker.ProfessionalSkills.BUILDER));
        //2 Welder, 3 ELECTRICIAN, 1 Carpenter, 3 Stonemason, 4 Builder

        olsa.addWorker(new Worker(650, Worker.ProfessionalSkills.WELDER, Worker.ProfessionalSkills.BUILDER, Worker.ProfessionalSkills.ELECTRIC));
        olsa.addWorker(new Worker(250, Worker.ProfessionalSkills.CARPENTER));
        olsa.addWorker(new Worker(400, Worker.ProfessionalSkills.WELDER, Worker.ProfessionalSkills.CARPENTER));
        olsa.addWorker(new Worker(900, Worker.ProfessionalSkills.values()));
        olsa.addWorker(new Worker(300, Worker.ProfessionalSkills.WELDER));
        //4 Welder, 2 ELECTRICIAN, 3 Carpenter, 1 Stonemason, 2 Builder
    }

    @Before
    public void setTender() {
        tender = new Tender("National Library");
        tender.addCandidate(iTechArt);
        tender.addCandidate(olsa);
        tender.addCandidate(mogilevTech);

        tender.addConditions(Worker.ProfessionalSkills.ELECTRIC, 2);
        tender.addConditions(Worker.ProfessionalSkills.BUILDER, 3);
        tender.addConditions(Worker.ProfessionalSkills.WELDER, 2);
    }


    @Test
    public void testAddCandidate() {
        String expected = "Mogilev Tech";
        int indexOfName = tender.getCandidates().indexOf(mogilevTech);
        String actual = tender.getCandidates().get(indexOfName).getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddConditions() {
        tender.addConditions(Worker.ProfessionalSkills.ELECTRIC, 2);
        int condition = tender.getNecessaryConditions().get(Worker.ProfessionalSkills.ELECTRIC);
        int expected = 2;
        Assert.assertEquals(expected, condition);
    }

    @Test
    public void testSearchMinInMap() {
        Map<String, Integer> result = new HashMap<>();
        result.put("Olsa", 700);
        result.put("itechArt", 900);
        String actual = tender.searchMinInMap(result);
        String expected = "Olsa";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFindApplicable() throws NoApplicableCandidateException {
        tender.findApplicable();
        int actual = tender.getResult().get("iTechArt");
        int expected = 2750;
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NoApplicableCandidateException.class)
    public void testFindApplicableWithApplicableCandidateException() throws NoApplicableCandidateException {
        Tender tender2 = new Tender("Some kind of tender");
        tender2.findApplicable();
    }

    @Test(expected = ModifyClosedTenderException.class)
    public void testFindApplicableWithModifyClosedTenderException() {
        tender.close();
        try {
            tender.findApplicable();
        } catch (NoApplicableCandidateException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void isActive() {
        boolean actual = tender.isActive();
        Assert.assertTrue(actual);
    }

    @Test
    public void close() {
        tender.close();
        boolean actual = tender.isActive();
        Assert.assertFalse(actual);
    }
}