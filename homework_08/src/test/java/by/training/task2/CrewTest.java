package by.training.task2;

import by.training.task2.exceptions.EmptyCrewException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CrewTest {
    Crew epam;

    @Before
    public void init() {
        epam = new Crew("epam");
    }

    @Test(expected = EmptyCrewException.class)
    public void getEstimateWithEmptyCrewException() {
        epam.getEstimate();
    }
}