package by.training.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WorkerTest {
    Worker worker;

    @Before
    public void initialization() {
        worker = new Worker(200, Worker.ProfessionalSkills.STONEMASON);
    }

    @Test
    public void getFinancialPosition() {
        int actual = worker.getFinancialPosition();
        int expected = 200;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void isApplicable() {
        boolean applicable = worker.isApplicable(Worker.ProfessionalSkills.STONEMASON);
        boolean expected = true;
        assertEquals(expected, applicable);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createWorkerWithIllegalArgumentException() {
        Worker worker = new Worker(-100, Worker.ProfessionalSkills.BUILDER);
    }
}