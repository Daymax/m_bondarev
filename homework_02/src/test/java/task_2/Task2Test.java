package task_2;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test for Task2 class.
 */

public class Task2Test {

    private int[] fibonacciResultArray;
    private int factorialResult;

    /**
     * Test for fibonacci method who is waiting for the right decision.
     */

    @Test
    public void fibonacci() {
        int expected = 3;
        fibonacciResultArray = Task2.fibonacci(5, 3);
        Assert.assertEquals(expected, fibonacciResultArray[4]);
    }

    /**
     * Test for factorial method who is waiting for the right decision.
     */

    @Test
    public void factorial(){
        int expected = 120;
        factorialResult = Task2.factorial(5, 3);
        Assert.assertEquals(expected, factorialResult);
    }

    /**
     * Test for fibonacci method who is waiting for the exception when choosing the wrong loopType.
     */

    @Test(expected = IllegalStateException.class)
    public void factorialWithIllegalException() {
        int expected = 1;
        factorialResult = Task2.factorial(2, 5);
        Assert.assertEquals(expected, factorialResult);
    }

    /**
     * Test for factorial method who is waiting for the exception when choosing the wrong loopType.
     */

    @Test(expected = IllegalStateException.class)
    public void fibonacciWithIllegalException(){
        int expected = 1;
        fibonacciResultArray = Task2.fibonacci(2, 8);
        Assert.assertEquals(expected, fibonacciResultArray[3]);
    }
}