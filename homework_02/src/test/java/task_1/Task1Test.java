package task_1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test for Task1 class.
 */

public class Task1Test {

    /**
     * Test for calculatedG method who is waiting for the right decision.
     */

    @Test
    public void calculatedG() {
        double expected = 19.739;
        double actual = Task1.calculateG(1, 1, 1, 1);
        Assert.assertEquals(expected, actual, 0.001);
    }

    /**
     * Test for calculatedG method who is waiting for the exception when division by zero occurs.
     */

    @Test(expected = ArithmeticException.class)
    public void calculatedGDivisionByZero() {
        int expected = 0;
        double actual = Task1.calculateG(1,0, 1,1);
        Assert.assertEquals(expected, actual, 0.001);
    }
}