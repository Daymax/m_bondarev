package task_1;

import java.util.Scanner;

/**
 * Main class.
 */

public class Task1 {

    /**
     * This is the main method which makes use of calculatedG method.
     *
     * @param args - Arguments
     */

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter 4 numbers: ");
        int a = scanner.nextInt();
        int p = scanner.nextInt();
        double m1 = scanner.nextDouble();
        double m2 = scanner.nextDouble();

        System.out.println(calculateG(a, p, m1, m2));
    }

    /**
     * Returns the number calculated by the formula.
     *
     * @param a  - int a parameter.
     * @param p  - int p parameter.
     * @param m1 - double m1 parameter.
     * @param m2 - double m2 parameter.
     * @return G calculated by the formula.
     */

    public static Double calculateG(int a, int p, double m1, double m2) {
        if ((Math.pow(p, 2) * (m1 + m2)) == 0) {
            throw new ArithmeticException();
        } else {
            return 4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3) / ((Math.pow(p, 2) * (m1 + m2))));
        }
    }
}
