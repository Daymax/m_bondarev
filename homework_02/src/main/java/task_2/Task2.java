package task_2;

import java.util.Scanner;

/**
 * @author Jericho
 * @version - 1.0
 */

public class Task2 {

    /**
     * Main method for choose mode.
     *
     * @param args - Arguments
     */

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("Choose algorithm(1 - fibonacci, 2 - factorial): ");
        int algorithmId = scan.nextInt();

        System.out.print("Choose loop(1 - while, 2 - do while, 3 - for): ");
        int loopType = scan.nextInt();

        System.out.print("Input number: ");
        int number = scan.nextInt();

        int factorialResult;
        int[] fibonacciResult;

        switch (algorithmId) {
            case (1):
                fibonacciResult = fibonacci(number, loopType);
                System.out.print("Fibonacci numbers: ");
                for (Integer element : fibonacciResult) {
                    System.out.print(element + "\t");
                }
                break;
            case (2):
                factorialResult = factorial(number, loopType);
                System.out.print("Factorial: " + factorialResult);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + algorithmId);
        }
    }

    /**
     * Method that calculate the factorial.
     *
     * @param number   - Incoming number
     * @param loopType - Type loop
     * @return - Factorial number
     */

    public static int factorial(int number, int loopType) {

        int result = 1;
        switch (loopType) {
            case (1):
                while (number > 0) {
                    result *= number;
                    number--;
                }
                break;
            case (2):
                do {
                    result *= number;
                    number--;
                } while (number > 0);
                break;
            case (3):
                for (; number > 0; number--) {
                    result *= number;
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + loopType);
        }
        return result;
    }

    /**
     * Method that return numeric Fibonacci values.
     *
     * @param number   - Incoming number
     * @param loopType - Type loop
     * @return - Array of Fibonacci numbers
     */

    public static int[] fibonacci(int number, int loopType) {

        int[] numbers = new int[number];
        numbers[0] = 0;
        numbers[1] = 1;
        int temp = 2;
        switch (loopType) {
            case (1):
                while (temp < number) {
                    numbers[temp] = numbers[temp - 1] + numbers[temp - 2];
                    temp++;
                }
                break;
            case (2):
                do {
                    numbers[temp] = numbers[temp - 1] + numbers[temp - 2];
                    temp++;
                } while (temp < number);
                break;
            case (3):
                for (; temp < number; temp++) {
                    numbers[temp] = numbers[temp - 1] + numbers[temp - 2];
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + loopType);
        }
        return numbers;
    }
}