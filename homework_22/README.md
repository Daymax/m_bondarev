# How to run

  Run command:
```sh
mvn jetty:run
```
Then go to:
```sh
localhost:8080/online-shop
```
You could login with:


| login        | password| 
| ------------- |:-------:|
| user      | user    |
| admin      | admin   |