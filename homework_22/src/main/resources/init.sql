create table if not exists users
(
    id       INTEGER auto_increment,
    username VARCHAR(255),
    password VARCHAR(255),
    role     VARCHAR(255),
    PRIMARY KEY (id)
);

create table if not exists goods
(
    id    INTEGER auto_increment,
    title VARCHAR(255),
    price INTEGER NOT NULL,
    PRIMARY KEY (id)
);

create table if not exists orders
(
    id          INTEGER auto_increment,
    total_price INTEGER NOT NULL,
    PRIMARY KEY (id),
);

create table if not exists orders_goods
(
    id integer auto_increment,
    order_id INTEGER not null ,
    good_id  INTEGER not null ,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) references orders (id),
    FOREIGN KEY (good_id) references goods (id)
);

create table if not exists users_orders
(
    user_id  INTEGER not null,
    order_id INTEGER not null,
    PRIMARY KEY (user_id, order_id),
    FOREIGN KEY (user_id) references users (id),
    FOREIGN KEY (order_id) references orders (id)
);

insert into users(username, password, role)
values ('user', 'user', 'USER');
insert into users(username, password, role)
values ('admin', 'admin', 'ADMIN');

insert into goods(title, price)
values ('Book', 10);
insert into goods(title, price)
values ('Pen', 2);
insert into goods(title, price)
values ('Pencil', 1);
insert into goods(title, price)
values ('Paper', 5);