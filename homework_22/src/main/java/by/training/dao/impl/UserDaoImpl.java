package by.training.dao.impl;

import by.training.dao.UserDao;
import by.training.entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * User DAO implementation.
 *
 * @author m_bondarev
 * @version 1.0
 * @see UserDao
 */
@Repository
public class UserDaoImpl implements UserDao {

    private final SessionFactory sessionFactory;

    public UserDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void addUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }

    @Override
    public User getUserByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
        Root<User> goodRoot = criteriaQuery.from(User.class);
        criteriaQuery.select(goodRoot)
                .where(cb.equal(goodRoot.get("username"), username));
        return session.createQuery(criteriaQuery).getResultList().stream()
                .findFirst()
                .orElse(null);
    }
}
