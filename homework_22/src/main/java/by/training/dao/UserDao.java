package by.training.dao;

import by.training.entities.User;

/**
 * User DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface UserDao {
    /**
     * Adds user to data source.
     *
     * @param user user
     */
    void addUser(User user);

    /**
     * Returns user by username.
     *
     * @param username username
     */
    User getUserByUsername(String username);
}
