package by.training.dao;

import by.training.entities.Good;

import java.util.List;

/**
 * Good DAO interface.
 *
 * @author m_bondarev
 * @version 1.0
 */
public interface GoodDao {
    /**
     * Returns good by id.
     *
     * @param id id
     * @return good by id
     */
    Good getGoodById(int id);

    /**
     * Returns list of all goods.
     *
     * @return list of goods
     */
    List<Good> getGoods();
}
