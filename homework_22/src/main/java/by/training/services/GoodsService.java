package by.training.services;

import by.training.dao.GoodDao;
import by.training.dao.impl.GoodDaoImpl;
import by.training.entities.Good;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Goods service class.
 *
 * @author m_bondarev
 * @version 1.0
 */

@Service
@Transactional
public class GoodsService {
    private final GoodDao goodDao;

    public GoodsService(GoodDaoImpl goodDao) {
        this.goodDao = goodDao;
    }

    /**
     * Returns good by id.
     *
     * @param id id
     * @return good by id
     */
    public Good getGoodById(int id) {
        return goodDao.getGoodById(id);
    }

    /**
     * Returns list of all goods.
     *
     * @return list of goods
     */
    public List<Good> getGoods() {
        return goodDao.getGoods();
    }
}
