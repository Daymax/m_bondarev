package by.training.services;

import by.training.dao.OrderDao;
import by.training.entities.Good;
import by.training.entities.Order;
import by.training.entities.User;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Order service class.
 *
 * @author m_bondarev
 * @version 1.0
 */
@Service
@Transactional
public class OrderService {

    private final OrderDao orderDao;

    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * Saves order to data source.
     *
     * @param user       user
     * @param addedGoods session added goods
     */
    public void saveOrder(User user, List<Good> addedGoods) {
        orderDao.saveOrder(user, addedGoods);
    }

    /**
     * Returns user's orders.
     *
     * @param user user
     * @return list of the orders
     */
    public List<Order> getUserOrders(User user) {
        return orderDao.getUserOrders(user).stream()
                .sorted(Comparator.comparing(Order::getId))
                .collect(Collectors.toList());
    }

    /**
     * Returns order by id.
     *
     * @param id id
     * @return order
     */
    public Order getOrderById(int id) {
        return orderDao.getOrderById(id);
    }
}
