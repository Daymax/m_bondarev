package by.training.entities;

import java.io.Serializable;
import java.util.List;

public class Order implements Serializable {
    private int id;
    private int totalPrice;

    private List<Good> goods;

    public Order() {
    }

    public Order(int id) {
        this.id = id;
    }

    public Order(List<Good> goods) {
        this.goods = goods;
    }

    public Order(int id, int totalPrice) {
        this.id = id;
        this.totalPrice = totalPrice;
    }

    public Order(int totalPrice, List<Good> goods) {
        this.totalPrice = totalPrice;
        this.goods = goods;
    }


    public Order(int id, int totalPrice, List<Good> goods) {
        this.id = id;
        this.totalPrice = totalPrice;
        this.goods = goods;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
