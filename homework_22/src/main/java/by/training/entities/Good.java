package by.training.entities;

import java.io.Serializable;
import java.util.Objects;

/**
 * Good class.
 *
 * @author m_bondarev
 * @version 1.0
 */
public class Good implements Serializable {
    private int id;
    private String title;
    private int price;

    private Order order;

    public Good() {
    }

    public Good(int id) {
        this.id = id;
    }

    public Good(String title) {
        this.title = title;
    }

    public Good(Order order) {
        this.order = order;
    }

    public Good(String title, int price) {
        this.title = title;
        this.price = price;
    }

    public Good(int price, Order order) {
        this.price = price;
        this.order = order;
    }

    public Good(int id, int price) {
        this.id = id;
        this.price = price;
    }

    public Good(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public Good(String title, Order order) {
        this.title = title;
        this.order = order;
    }

    public Good(int id, String title, int price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public Good(String title, int price, Order order) {
        this.title = title;
        this.price = price;
        this.order = order;
    }

    public Good(int id, String title, int price, Order order) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.order = order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Good good = (Good) o;
        return id == good.id &&
                price == good.price &&
                Objects.equals(title, good.title) &&
                Objects.equals(order, good.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, price, order);
    }
}
