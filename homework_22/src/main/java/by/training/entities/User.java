package by.training.entities;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;


public class User implements Serializable {
    private int id;
    private String username;
    private String password;
    private String role;

    private Set<Order> orders;

    public User() {
    }

    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public User(int id, String username, String password, String role, Set<Order> orders) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.orders = orders;
    }

    public User(String username, String password, String role, Set<Order> orders) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.orders = orders;
    }

    public User(String username, String password, String role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public User(String password, String role, Set<Order> orders) {
        this.password = password;
        this.role = role;
        this.orders = orders;
    }

    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public User(int id, String password, String role, Set<Order> orders) {
        this.id = id;
        this.password = password;
        this.role = role;
        this.orders = orders;
    }

    public User(int id, String password, Set<Order> orders) {
        this.id = id;
        this.password = password;
        this.orders = orders;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(role, user.role) &&
                Objects.equals(orders, user.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, role, orders);
    }
}
